import {create} from "zustand";

type StateProps = {
  radioPlaying: any;
  setRadioPlaying: (radioPlaying: string) => void;
  audioRef: React.MutableRefObject<HTMLVideoElement | null> | null;
  setAudioRef: (audioRef: any) => void;
  radioInfo: any;
  setRadioInfo: (audioRef: any) => void;
};

export const playStore = create<StateProps>((set) => ({
  radioPlaying: "",
  setRadioPlaying: (radioPlaying) => set({ radioPlaying }),
  audioRef: null,
  setAudioRef: (audioRef: any) => set({ audioRef }),
  radioInfo: {},
  setRadioInfo: (radioInfo) => set({ radioInfo }),
}));
