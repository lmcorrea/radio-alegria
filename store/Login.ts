import { authenticatedUrl, baseApiUrl } from "@/services/api";
import {
  RegisterServiceResponse,
  UserType,
} from "@/types/RegisterServiceResponse";
import { RevalidateTokenServiceResponse } from "@/types/RevalidateTokenServiceResponse";
import { create } from "zustand";

export const emptyUser: UserType = {
  id: 0,
  user_type: "",
  type: "",
  name_corporate: "",
  name_fantasy: "",
  register_type: "",
  cnpj: "",
  cpf: "",
  email: "",
  key_remote_access: null,
  other_emails: "",
  financial_name: null,
  financial_email: null,
  financial_observations: null,
  payment_due_date: "",
  payment_method: "",
  requester_name: null,
  requester_email: null,
  requester_bond: "",
  domain_slug: null,
  slogan: null,
  power_kw: 0,
  class: "",
  link: "",
  link_streaming: null,
  link_server_rds: null,
  type_server_rds: "",
  link_radio_data_system: null,
  radio_data_system_id: null,
  spotlight: false,
  super_spotlight: false,
  has_audio: false,
  show_radios_online: false,
  status_show_radios_online: 0,
  about: "",
  observations: null,
  incomplete_registration: false,
  incomplete_registration_alert: null,
  run_by: 0,
  status: 0,
  show_music_play: false,
  registered_by: 0,
  first_access: "",
  last_access: "",
  validate_access: "",
  uuid: "",
  is_public: 0,
  notify_content_update: false,
  receive_mailing: false,
  show_logo_radio_subscriber: false,
  accepted_terms_of_use: false,
  advertiser_segment_id: null,
  enable_automatic_product_block: false,
  request_from: "",
  created: "",
  modified: "",
  clients_emails: [],
  location: {
    id: 0,
    branch_id: null,
    foreign_key: 0,
    model: "",
    cep: null,
    address: "",
    district: "",
    number: "",
    complement: null,
    reference: null,
    phone_1: "",
    phone_2: null,
    country_id: 0,
    city_id: 0,
    state_id: 0,
    city_name: null,
    state_name: null,
    lon: null,
    lat: null,
    run_by: 0,
    active: false,
    created: "",
    modified: "",
    city: {
      id: 0,
      state_id: 0,
      uf: "",
      name: "",
      is_capital: false,
    },
    state: {
      id: 0,
      country_id: 0,
      uf: "",
      name: "",
    },
    country: {
      id: 0,
      name: "",
      name_en: "",
      alias: "",
      bacen: 0,
    },
  },
  token: "",
  client_api: null,
  other_cities: [],
  radio_segments: null,
  preference: {
    musics: {
      rhythms: [],
      years: [],
      nationalities: [],
      tag_styles: [],
    },
    products: {
      tags: [],
    },
  },
  cover: {
    uri: "",
  },
  has_favorites: false,
  radio_type: "",
  accessProfileScopes: [],
};

interface UserPayload {
  name: string;
  email: string;
  password: string;
  phone: string;
  state: string;
  city: string;
  requestFrom: "App" | "Web";
}

type StateProps = {
  handleRevalidateToken: () => Promise<RevalidateTokenServiceResponse>;

  handleLogIn: (
    email: string,
    password: string,
    from: "App" | "Web"
  ) => Promise<any>;

  user: UserType;
  setUser: (user: UserType) => void;

  handleRegisterUser: (data: UserPayload) => Promise<RegisterServiceResponse>;
};

export const loginStore = create<StateProps>((set) => ({
  user: emptyUser,
  setUser: (user) => set({ user }),

  handleRevalidateToken: async () => {
    try {
      const { data } = await authenticatedUrl.get(
        "/radio-store/manager-clients/get-client-logged"
      );

      return data as RevalidateTokenServiceResponse;
    } catch (error) {
      return error;
    }
  },

  handleLogIn: async (email, password, from) => {
    try{
      const {data} = await baseApiUrl.post("radio-store/manager-clients/authorize-client-id",
        {
          email,
          password,
          login_from: from,
        }
      );
      console.log(data, "response");
      return data;

    } catch (error) {
      throw error;
    }
  },

  handleRegisterUser: async ({
    name,
    email,
    password,
    phone,
    state,
    city,
    requestFrom,
  }) => {
    const { data } = await baseApiUrl.post(
      "radio-store/manager-clients/register-client",
      {
        user_type: "Listener",
        name_fantasy: name,
        name_corporate: name,
        email: email,
        confirm_password: password,
        password: password,
        location: {
          country_id: 1,
          phone_1: phone,
          state: state,
          city_id: city,
        },
        registered_by: "90",
        request_from: requestFrom,
      }
    );

    return data;
  },
}));
