import React, { useEffect, useRef, useState } from "react";
import {
  ScrollView,
  StatusBar,
  Image,
  Platform,
  View,
  Pressable,
  Dimensions,
} from "react-native";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { BackButton, Text } from "@/components/";
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { Colors } from "@/constants/Colors";
import { AntDesign } from "@expo/vector-icons";
import { superAudioapiApp } from "@/services/api";
import { format } from "date-fns";
import { router, useLocalSearchParams } from "expo-router";

export default function GaleriaID() {
  const isMobile = Dimensions.get("window").width < 768;
  const ScrollRef = useRef<ScrollView>(null);
  const ScrollWidthOffset = useRef(0);
  const [images, setImages] = useState<any>();

  const { id } = useLocalSearchParams();

  useEffect(() => {
    async function getData() {
      try {
        const { data } = await superAudioapiApp.get(
          `picture-albums/get-picture-album/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00/${id}/`
        );
        setImages(data.data);
      } catch (error) {}
    }
    getData();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        headerTitle={
          <Text
            style={{
              fontSize: isMobile
                ? responsiveScreenFontSize(3)
                : responsiveScreenFontSize(4),
              color: Colors.green,
            }}
            fontWeight="bold">
            #TÔ
            <Text fontWeight="regular" style={{ color: Colors.green }}>
              NA
            </Text>
            ALEGRIA
          </Text>
        }>
        <View
          style={{
            height: isMobile ? 500 : 800,
            gap: 20,
            alignItems: "center",
          }}>
          {images && (
            <View
              style={{
                height: isMobile ? 300 : 550,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Text
                fontWeight="bold"
                style={{
                  fontSize: responsiveScreenFontSize(2),
                }}>
                {images.title}
              </Text>
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(1.6),
                }}>
                {format(images.created, "dd/MM/yyyy")}
              </Text>
              <Pressable
                style={{
                  width: 30,
                  height: 30,
                  position: "absolute",
                  borderRadius: 15,
                  left: isMobile ? 15 : "5%",
                  top: "50%",
                  zIndex: 5,
                  opacity: 0.6,
                }}
                onPress={() => {
                  ScrollWidthOffset.current -= isMobile ? 200 : 750;

                  if (ScrollWidthOffset.current < 0)
                    ScrollWidthOffset.current = 0;
                  ScrollRef.current?.scrollTo({
                    x: ScrollWidthOffset.current,
                    y: 0,
                    animated: true,
                  });
                }}>
                <AntDesign
                  name="leftcircleo"
                  size={isMobile ? 30 : 40}
                  color="white"
                />
              </Pressable>
              <ScrollView
                ref={ScrollRef}
                horizontal
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                snapToInterval={isMobile ? 200 : 750}
                snapToAlignment="center"
                snapToOffsets={[...Array(images.imgs.length).keys()].map( (i) => i * (isMobile ? 200 : 750))}
                decelerationRate="fast"
                snapToEnd={false}
                style={{
                  width: isMobile ? 200 : 770,
                  height: isMobile ? 200 : 500,
                }}
                contentContainerStyle={{
                  marginTop: 20,
                }}>
                {images.imgs.map((item, index) => (
                  <Image
                    key={index}
                    source={{
                      uri: item.image,
                    }}
                    resizeMode="contain"
                    style={{
                      height: isMobile ? 200 : 400,
                      width: isMobile ? 200 : 750,
                      //backgroundColor: Colors.lightGreen,
                    }}
                  />
                ))}
              </ScrollView>
              <Pressable
                style={{
                  width: 30,
                  height: 30,
                  position: "absolute",
                  borderRadius: 15,
                  right: isMobile ? 15 : "10%",
                  top: "50%",
                  zIndex: 5,
                  opacity: 0.6,
                }}
                onPress={() => {
                  ScrollWidthOffset.current += isMobile ? 200 : 750;
                  ScrollRef.current?.scrollTo({
                    x: ScrollWidthOffset.current,
                    animated: true,
                  });
                }}>
                <AntDesign
                  name="rightcircleo"
                  size={isMobile ? 30 : 40}
                  color="white"
                />
              </Pressable>
            </View>
          )}
          <View style={{left: -15}}>
            <BackButton textColor="white" />
          </View>
        </View>
      </PageLayout>
    </ScrollView>
  );
}
