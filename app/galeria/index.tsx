import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StatusBar,
  Image,
  Platform,
  View,
  Pressable,
  Dimensions,
} from "react-native";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { Text } from "@/components/";
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { Colors } from "@/constants/Colors";
import { router } from "expo-router";
import { superAudioapiApp } from "@/services/api";
import { format } from "date-fns";
import { moderateScale } from "react-native-size-matters";

export default function Galeria() {
  const isMobile = Dimensions.get("window").width < 768;
  const [pictureAlbums, setPictureAlbums] = useState<any[]>([]);

  useEffect(() => {
    async function getData() {
      try {
        const { data } = await superAudioapiApp.get(
          "picture-albums/list-picture-albums/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00/"
        );
        setPictureAlbums(data.data);
      } catch (error) {}
    }
    getData();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        headerTitle={
          <Text
            style={{
              fontSize: isMobile
                ? responsiveScreenFontSize(3)
                : responsiveScreenFontSize(4),
              color: Colors.green,
            }}
            fontWeight="bold">
            #TÔ
            <Text fontWeight="regular" style={{ color: Colors.green }}>
              NA
            </Text>
            ALEGRIA
          </Text>
        }>
        <View
          style={{
            marginTop: 80,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: responsiveScreenHeight(10),
            flexWrap: "wrap",
            gap: isMobile ? 20 : 60,
            marginHorizontal: isMobile ? 20 : 70,
          }}>
          {pictureAlbums?.map((item, index) => (
            <View
              key={index}
              style={{
                alignItems: "center",
                justifyContent: "center",
                gap: isMobile ? 5 : 10,
              }}>
              <Text
                fontWeight="bold"
                style={{
                  fontSize: responsiveScreenFontSize(1.6),
                }}>
                {item.title}
              </Text>
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(1.4),
                }}>
                {format(item.created, "dd/MM/yyyy")}
              </Text>
              <Pressable onPress={() => router.push(`galeria/${item.id}`)}>
                {item.cover && typeof item.cover === "string" && (
                  <Image
                    source={{
                      uri: item.cover,
                    }}
                    style={{
                      height: isMobile ? 130 : 250,
                      width: isMobile ? 130 : 250,
                      borderRadius: 10,
                    }}
                    resizeMode="contain"
                  />
                )}
              </Pressable>
            </View>
          ))}
        </View>
      </PageLayout>
    </ScrollView>
  );
}
