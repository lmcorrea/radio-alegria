import {
  Image,
  View,
  ImageBackground,
  ScrollView,
  StatusBar,
  Pressable,
} from "react-native";
import Header from "@/components/Header";
import SwiperFlatList from "react-native-swiper-flatlist";
import Input from "@/components/Input";
import Icon from "react-native-vector-icons/FontAwesome";
import { useEffect, useState } from "react";
import React from "react";
import { superAudioapiApp } from "@/services/api";
import responsiveSizes from "@/utils/responsiveSizes";
import { Colors } from "@/constants/Colors";
import { Text } from "@/components";

type AgronegocioType = {
  id: number;
  title: string;
  img: string;
};

export default function Receitas() {
  //const isMobile = Dimensions.get("window").width < 768;

  const [filterInput, setFilterInput] = useState("");
  const [agronegocio, setAgronegocio] = useState<AgronegocioType[]>([]);
  const [banners, setBanners] = useState<any[]>([]);
  const { isMobile, RW, RH, RF } = responsiveSizes();

  useEffect(() => {
    getAgronegocio();
    getBanners();
  }, []);

  async function getAgronegocio() {
    try {
      const { data } = await superAudioapiApp.get(
        "want-adds/list-want-adds/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00/"
      );
      setAgronegocio(data.data);
    } catch (error) {
      console.log(error);
    }
  }

  async function getBanners() {
    try {
      const { data } = await superAudioapiApp.get(
        "banners/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00"
      );
      console.log(data);
      setBanners(data.data);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <View style={{ zIndex: 1 }}>
        <SwiperFlatList
          autoplay
          autoplayLoopKeepAnimation={true}
          showPagination
          data={banners}
          renderItem={({ item }) => (
            <Pressable>
              <Image
                source={{
                  uri: item.dir,
                }}
                resizeMode="cover"
                style={{
                  width: RW(110),
                  height: isMobile ? RH(20) : RH(50),
                }}
              />
            </Pressable>
          )}
          pagingEnabled
          paginationDefaultColor="transparent"
          paginationStyleItem={{
            width: RF(1),
            height: RF(1),
            borderRadius: 20,
            marginHorizontal: 7,
            borderWidth: 2,
            borderColor: "#FFF",
          }}
          paginationStyle={{
            alignSelf: "flex-end",
            right: RF(10),
          }}
        />
      </View>
      <ImageBackground
        source={
          isMobile
            ? require("@/assets/images/receitas/Ativo 8.png")
            : require("@/assets/images/receitas/Ativo 4.png")
        }
        style={{
          width: "100%",
          alignItems: "center",
          height: "auto",
        }}>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            backgroundColor: "white",
            gap: 10,
            width: "80%",
            marginTop: RF(-2),
            paddingTop: RF(5),
            paddingBottom: isMobile ? RF(1) : RF(1),
          }}>
          <Image
            source={
              isMobile
                ? require("@/assets/images/agronegocio/agro_logo_mob.png")
                : require("@/assets/images/agronegocio/agro_logo_desk.png")
            }
            style={{
              width: RF(50),
              height: RF(5),
            }}
            resizeMode="contain"
          />
          {agronegocio?.length > 0 ? (
            <>
              <View
                style={{
                  marginTop: 20,
                  marginBottom: 30,
                  width: "50%",
                }}>
                <Input
                  icon={<Icon name="search" size={20} color="gray" />}
                  setText={(text) => {
                    setFilterInput(text);
                  }}
                  value={filterInput}
                  WrapperProps={{
                    borderWidth: 2,
                    width: "100%",
                  }}
                  StyleInputProps={{
                    fontSize: RF(1.2),
                  }}
                  placeholder="Buscar um produto ou serviço"
                />
              </View>
              <View
                style={{
                  width: "100%",
                  marginBottom: 50,
                  padding: 20,
                  gap: 15,
                }}>
                {agronegocio
                  .filter((negocio) => {
                    return negocio.title
                      .toLowerCase()
                      .includes(filterInput.toLowerCase());
                  })
                  .map((negocio) => (
                    <Card
                      key={negocio.id}
                      item={
                        {
                          id: negocio.id,
                          headline: negocio.title,
                          links: { cover: negocio.img },
                        } as any
                      }
                      url="agronegocio"
                    />
                  ))}
              </View>
            </>
          ) : (
            <View
              style={{
                width: "100%",
                height: RH(30),
              }}>
              <Text
                fontWeight="bold"
                style={{
                  color: "black",
                  fontSize: 24,
                  textAlign: "center",
                  marginTop: 20,
                }}>
                Não há produtos ou serviços
              </Text>
            </View>
          )}
        </View>
      </ImageBackground>
    </ScrollView>
  );
}
