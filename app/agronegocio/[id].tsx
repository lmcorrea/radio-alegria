import { router, useLocalSearchParams } from "expo-router";
import {
  Image,
  Platform,
  View,
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Pressable,
  Linking,
} from "react-native";
import {
  responsiveFontSize,
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { BackButton, Text } from "@/components";
import { Colors } from "@/constants/Colors";
import { useEffect, useState } from "react";
import { superAudioapiApp } from "@/services/api";
import { moderateScale, scale } from "react-native-size-matters";
import {
  FontAwesome,
  FontAwesome5,
  FontAwesome6,
  Fontisto,
  Ionicons,
} from "@expo/vector-icons";
import SwiperFlatList from "react-native-swiper-flatlist";

const isMobile = Dimensions.get("window").width < 768;

export default function Receita() {
  const { width } = Dimensions.get("window");
  const { id } = useLocalSearchParams();
  const [agronegocio, setAgronegocio] = useState();

  useEffect(() => {
    async function getAgronegocio() {
      try {
        const { data } = await superAudioapiApp.get(
          `want-adds/get-want-add/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00/${id}`
        );
        setAgronegocio(data.data);
      } catch (error) {
        console.log(error);
      }
    }
    getAgronegocio();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        headerTitle={
          <Image
            source={
              isMobile
                ? require("@/assets/images/agronegocio/agro_logo_mob.png")
                : require("@/assets/images/agronegocio/agro_logo_desk.png")
            }
            style={{
              width: isMobile
                ? responsiveScreenWidth(60)
                : responsiveScreenWidth(40),
              height: isMobile
                ? responsiveScreenHeight(5)
                : responsiveScreenHeight(10),
            }}
            resizeMode="contain"
          />
        }>
        {agronegocio && (
          <>
            <View
              style={{
                width: isMobile
                  ? responsiveScreenWidth(70)
                  : responsiveScreenWidth(55),
                marginBottom: 20,
              }}>
              <SwiperFlatList
                autoplay
                autoplayLoopKeepAnimation={true}
                showPagination
                data={[agronegocio.img]}
                renderItem={({ item }: any) => (
                  <Image
                    source={{
                      uri: item,
                    }}
                    style={{
                      width: isMobile
                        ? responsiveScreenWidth(70)
                        : responsiveScreenWidth(55),
                      height: isMobile ? 180 : 350,
                      resizeMode: "stretch",
                    }}
                  />
                )}
                pagingEnabled
                paginationDefaultColor="transparent"
                paginationStyleItem={{
                  width: scale(10),
                  height: scale(10),
                  borderRadius: 20,
                  marginHorizontal: 7,
                  borderWidth: 2,
                  borderColor: "#FFF",
                }}
                paginationStyle={{
                  alignSelf: "flex-end",
                  right: scale(20),
                }}
              />
            </View>

            <Text
              style={{
                marginBottom: responsiveScreenHeight(4),
                color: Colors.boldGreen,
                fontWeight: "bold",
                fontSize: moderateScale(30),
              }}>
              {agronegocio.title}
            </Text>
            <Text
              fontWeight="light"
              style={{
                color: "black",
                fontSize: moderateScale(16),
              }}>
              {agronegocio.description}
            </Text>

            <View
              style={{
                marginTop: 50,
              }}>
              <Text
                fontWeight="thin"
                style={{
                  marginBottom: responsiveScreenHeight(4),
                  color: Colors.boldGreen,
                  fontSize: moderateScale(18),
                  textAlign: "center",
                }}>
                ENTRE EM CONTATO
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                flexWrap: "wrap",
                width: "120%",
                gap: 10,
              }}>
              <Pressable
                onPress={async () => {
                  try {
                    const url = `whatsapp://send?phone=${agronegocio.phone}`;
                    const suporta = await Linking.canOpenURL(url);
                    if (suporta) {
                      Linking.openURL(url);
                    } else {
                      Linking.openURL(
                        "https://play.google.com/store/apps/details?id=com.whatsapp"
                      );
                    }
                  } catch (error) {
                    console.log("erro");
                  }
                }}
                style={{
                  width: "45%",
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Image
                  source={require("@/assets/images/agronegocio/whatsapp.png")}
                  style={{ width: "100%", height: moderateScale(45) }}
                  resizeMode="stretch"
                />
                <Text
                  style={{
                    position: "absolute",
                    color: "white",
                    fontSize: responsiveScreenFontSize(1.3),
                    right: responsiveScreenWidth(7),
                  }}>
                  WHATSAPP
                </Text>
              </Pressable>
              <Pressable
                onPress={async () => {
                  try {
                    const url = agronegocio.link;
                    Linking.openURL(url);
                  } catch (error) {
                    console.log("erro");
                  }
                }}
                style={{
                  width: "45%",
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Image
                  source={require("@/assets/images/agronegocio/site.png")}
                  style={{ width: "100%", height: moderateScale(45) }}
                  resizeMode="stretch"
                />
                <Text
                  style={{
                    position: "absolute",
                    color: "white",
                    fontSize: responsiveScreenFontSize(1.5),
                    right: isMobile ?  responsiveScreenWidth(14) : responsiveScreenWidth(10),
                  }}>
                  SITE
                </Text>
              </Pressable>
            </View>

            <View style={{ width: "100%", marginBottom: 50 }}>
              <BackButton onPress={() => router.back()} />
            </View>
          </>
        )}
      </PageLayout>
    </ScrollView>
  );
}
