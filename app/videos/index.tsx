import {
  Image,
  Platform,
  View,
  ImageBackground,
  Dimensions,
  ScrollView,
  StatusBar,
  PixelRatio,
  TextInput,
  Pressable,
} from "react-native";
import React, { useEffect, useRef, useState } from "react";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import VideoCard from "@/components/videos/videoCard";
import { AntDesign } from "@expo/vector-icons";
import { superAudioapiApp } from "@/services/api";
import { moderateScale } from "react-native-size-matters";

export type Video = {
  id: number;
  title: string;
  description: string;
  video_link: string;
  date_publish_init: string;
  category_multimedia: {
    name: string;
  };
  time: number;
  links: {
    cover: string;
    view: string;
  };
};

export default function Videos() {
  const isMobile = Dimensions.get("window").width < 768;
  const ScrollRef = useRef<ScrollView>(null);
  const ScrollWidthOffset = useRef<number>(0);
  const [videos, setVideos] = useState<Video[]>([]);

  const { width } = Dimensions.get("window");

  const scale = (size: number) => {
    const scale = (width / PixelRatio.get()) * (size / 100);

    if (isMobile) return scale * 0.7;
    return scale;
  };

  useEffect(() => {
    async function data() {
      try {
        const { data } = await superAudioapiApp.get(
          "gallery/videos/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00"
        );
        setVideos(data.data);
      } catch (error) {
        console.log(error);
      }
    }
    data();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        headerTitle={
          <Image
            source={
              isMobile
                ? require("@/assets/images/videos.png")
                : require("@/assets/images/videos.png")
            }
            style={{
              width: isMobile
                ? responsiveScreenWidth(60)
                : responsiveScreenWidth(40),
              height: isMobile
                ? responsiveScreenHeight(5)
                : responsiveScreenHeight(10),
            }}
            resizeMode="contain"
          />
        }>
        <View
          style={{
            width: isMobile ? "95%" : scale(68),
            marginBottom: 70,
            alignItems: videos.length > 2 ? "stretch" : "center",
          }}>
          <Pressable
            style={{
              width: 30,
              height: 30,
              position: "absolute",
              borderRadius: 15,
              left: 30,
              top: isMobile ? "60%" : "50%",
              zIndex: 5,
            }}
            onPress={() => {
              ScrollRef.current?.scrollTo({
                x: 0,
                y: 0,
                animated: true,
              });
            }}>
            <AntDesign
              name="leftcircleo"
              size={moderateScale(30)}
              color="white"
            />
          </Pressable>
          <ScrollView
            ref={ScrollRef}
            horizontal
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              columnGap: 20,
            }}>
            {videos.map((video) => (
              <VideoCard key={video.id} video={video} />
            ))}
          </ScrollView>
          <Pressable
            style={{
              width: 30,
              height: 30,
              position: "absolute",
              borderRadius: 15,
              right: 30,
              top: isMobile ? "60%" : "50%",
              zIndex: 5,
            }}
            onPress={() => {
              ScrollWidthOffset.current += isMobile ? 100 : scale(22);

              ScrollRef.current?.scrollTo({
                x: ScrollWidthOffset.current,
                animated: true,
              });
            }}>
            <AntDesign
              name="rightcircleo"
              size={moderateScale(30)}
              color="white"
            />
          </Pressable>
        </View>
      </PageLayout>
    </ScrollView>
  );
}
