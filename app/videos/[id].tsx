import React, { useEffect, useRef, useState } from "react";
import {
  ScrollView,
  Dimensions,
  StatusBar,
  Image,
  Platform,
} from "react-native";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { View } from "react-native";
import { BackButton, Text } from "@/components";
import { superAudioapiApp } from "@/services/api";
import { moderateScale } from "react-native-size-matters";
import { useLocalSearchParams } from "expo-router";
import WebView from "react-native-webview";
import * as ScreenOrientation from "expo-screen-orientation";

export default function Video() {
  const isMobile = Dimensions.get("window").width < 768;
  const { id } = useLocalSearchParams();
  const [video, setVideo] = useState<any>();
  const [isPortrait, setIsPortrait] = useState(true);
  const { width } = Dimensions.get("window");

  async function getVideo() {
    try {
      const { data } = await superAudioapiApp.get(
        `gallery/videos/${id}/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00`
      );
      setVideo(data.data);
    } catch (error) {
      console.log("Error at getVideo", error);
    }
  }

  useEffect(() => {
    getVideo();
  }, []);

  useEffect(() => {
    ScreenOrientation.unlockAsync();

    const orientationChangeHandler = ({ orientationInfo }) => {
      if (orientationInfo?.orientation == 1) {
        return setIsPortrait(true);
      }
      setIsPortrait(false);
    };

    const subscription = ScreenOrientation.addOrientationChangeListener(
      orientationChangeHandler,
    );

    return () => {
      ScreenOrientation.removeOrientationChangeListener(subscription);
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);
    };
  }, []);

  function extractYouTubeID(url: String) {
    const patterns = [
      /youtube\.com\/watch\?v=([a-zA-Z0-9_-]+)/,
      /youtube\.com\/shorts\/([a-zA-Z0-9_-]+)/,
      /youtu\.be\/([a-zA-Z0-9_-]+)/,
    ];

    for (const pattern of patterns) {
      const match = url.match(pattern);
      if (match) {
        return match[1];
      }
    }

    return null;
  }
  //

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        headerTitle={
          <Image
            source={
              isMobile
                ? require("@/assets/images/videos.png")
                : require("@/assets/images/videos.png")
            }
            style={{
              width: isMobile
                ? responsiveScreenWidth(60)
                : responsiveScreenWidth(40),
              height: isMobile
                ? responsiveScreenHeight(5)
                : responsiveScreenHeight(10),
            }}
            resizeMode="contain"
          />
        }>
        <BackButton textColor="white" />
        {video && (
          <View
            style={{
              marginBottom: "10%",
              marginTop: 20,
              //borderWidth: 2,
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
            }}>
            {Platform.OS === "web" ? (
              <iframe
                width={isMobile ? "90%" : "70%"}
                height="500"
                src={`https://www.youtube.com/embed/${extractYouTubeID(
                  video.video_link
                )}`}
                title={video?.title}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen></iframe>
            ) : (
              <WebView
                source={{
                  html: `
                    <body style="margin: 0; padding: 0; align-items:"center; width:"500">
                      <iframe
                        width="${1000}"
                        height="500"
                        src="https://www.youtube.com/embed/${extractYouTubeID(
                          video.video_link
                        )}"
                        title="${video?.title}"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                        fullscreen
                      ></iframe>
                    </body>
                `,
                }}
                style={{
                  flex:1,
                  width: width,
                  height: isPortrait ? responsiveScreenWidth(100) : responsiveScreenWidth(60),
                  backgroundColor: "transparent",
                  alignSelf: "center",
                  justifyContent: "center",
                  justifySelf: "center",
                  borderColor: "red",
                  alignItems: "center",
                }}
              />
            )}
          </View>
        )}
      </PageLayout>
    </ScrollView>
  );
}
