import { View, Dimensions, Pressable } from "react-native";
import React from "react";

import Input from "@/components/Input";
import Text from "@/components/Text";
import { baseApiUrl, superAudioapiApp } from "@/services/api";
import responsiveSizes from "@/utils/responsiveSizes";
import { Colors } from "@/constants/Colors";

export default function InputFields() {
  const [assunto, setAssunto] = React.useState("");
  const [nome, setNome] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [telefone, setTelefone] = React.useState("");
  const [mensagem, setMensagem] = React.useState("");

  const { isMobile, RF, RH, RW } = responsiveSizes();

  const submit = async () => {
    if (!assunto || !nome || !email || !telefone || !mensagem) {
      alert("Preencha todos os campos");
      return;
    }

    try {
      await baseApiUrl.post(
        "api/listener-messages/save-contact/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00/",
        {
          subject: assunto,
          name: nome,
          message: mensagem,
          email: email,
          phone: telefone,
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View
      style={{
        width: isMobile ? "80%" : "60%",
        gap: 10,
        marginBottom: RW(1),
      }}>
      <Input
        placeholder="Assunto"
        setText={setAssunto}
        WrapperProps={{
          backgroundColor: "white",
        }}
      />
      <Input
        placeholder="Nome completo"
        setText={setNome}
        WrapperProps={{
          backgroundColor: "white",
        }}
      />
      <Input
        placeholder="Seu melhor e-mail"
        setText={setEmail}
        WrapperProps={{
          backgroundColor: "white",
        }}
      />
      <Input
        placeholder="Telefone para contato"
        value={telefone}
        setText={(text) => {
          let value = text;
          if (!value) return "";
          value = value.replace(/\D/g, "");
          value = value.replace(/(\d{2})(\d)/, "($1) $2");
          value = value.replace(/(\d)(\d{4})$/, "$1-$2");
          setTelefone(value);
        }}
        WrapperProps={{
          backgroundColor: "white",
        }}

      />
      <Input
        placeholder="Mensagem"
        setText={setMensagem}
        WrapperProps={{
          backgroundColor: "white",
          height: RF(15),
          width: "100%",
          borderRadius: 20,
        }}
        StyleInputProps={{
          height: RF(13),
          width: "98%",
        }}
        InputProps={{
          multiline: true,
          numberOfLines: 10,
          textAlignVertical: "top",
          maxLength: 900,
        }}
      />
      <Pressable style={{ alignSelf: "flex-end" }} onPress={() => submit()}>
        <View
          style={{
            backgroundColor: Colors.yellow,
            paddingVertical: RW(1),
            paddingHorizontal: isMobile ? RW(10) : RW(6),
          }}>
          <Text style={{ fontSize: RF(1.4) }}>Enviar</Text>
        </View>
      </Pressable>
    </View>
  );
}
