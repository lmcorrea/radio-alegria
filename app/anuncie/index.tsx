import { Dimensions, ScrollView, StatusBar, View } from "react-native";
import React, { useRef } from "react";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { Text } from "@/components";
import { Colors } from "@/constants/Colors";
import InputFields from "./InputFields";
import responsiveSizes from "@/utils/responsiveSizes";

export default function Anuncie() {
  const { width } = Dimensions.get("window");
  const { isMobile, RF, RH, RW } = responsiveSizes();

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        headerTitle={
          <>
            <Text
              style={{
                textAlign: "center",
                color: Colors.boldGreen,
                fontWeight: "bold",
                fontSize: isMobile ? RF(2) : RF(2.6),
                marginTop: isMobile ? RH(2) : RH(3),
              }}>
              VAMOS CONVERSAR ?
            </Text>
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontSize: RF(1.6),
                marginTop: isMobile ? RW(2) : RW(1),
              }}>
              Mande sua mensagem e entraremos em contato o mais rapido possível
            </Text>
          </>
        }>
        <View style={{
          width: "100%",
          alignItems: "center",
          paddingBottom: isMobile ? RW(10) : RW(2),
        }}>
          <InputFields />
        </View>
      </PageLayout>
    </ScrollView>
  );
}
