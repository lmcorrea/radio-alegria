import { router, useLocalSearchParams } from "expo-router";
import {
  Image,
  Platform,
  View,
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";
import {
  responsiveFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { BackButton, Text } from "@/components";
import { Colors } from "@/constants/Colors";
import NewsCard from "@/components/noticias/NewsCard";
import { useEffect } from "react";
import { superAudioapiApp } from "@/services/api";
import { WebView } from "react-native-webview";
import { scale, verticalScale } from "react-native-size-matters";

const isMobile = Dimensions.get("window").width < 768;

type NoticiaProps = {
  audios: any[];
  author: string;
  category_multimedia: {
    name: string;
  };
  date_publish_init: string;
  description: string;
  headline: string;
  highlight: number;
  links: {
    cover: string;
  };
  text: string;
  videos: any[];
};

export default function Noticia() {
  const { width } = Dimensions.get("window");
  const [noticia, setNoticia] = useState<NoticiaProps>();
  const { id } = useLocalSearchParams();

  useEffect(() => {
    async function getNoticia() {
      try {
        const { data } = await superAudioapiApp.get(
          `news/${id}/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00/`
        );

        setNoticia(data.news);
      } catch (error) {
        console.log(error);
      }
    }

    getNoticia();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      {noticia && (
        <PageLayout
          whitePage={false}
          headerTitle={
            <Image
              source={
                isMobile
                  ? require("@/assets/images/noticias/logo_noticias_mob.png")
                  : require("@/assets/images/noticias/logo_noticias_desk.png")
              }
              style={{
                width: isMobile
                  ? responsiveScreenWidth(60)
                  : responsiveScreenWidth(40),
                height: isMobile
                  ? responsiveScreenHeight(10)
                  : responsiveScreenHeight(10),
              }}
              resizeMode="contain"
            />
          }
        >
          <Text
            style={{
              color: "white",
              marginTop: verticalScale(8),
              width: isMobile
                ? responsiveScreenWidth(90)
                : responsiveScreenWidth(60),
              fontSize: scale(25),
            }}
          >
            {noticia.headline}
          </Text>

          <Text
            style={{
              color: "white",
              textAlign: "center",
              flexShrink: 1,
              marginTop: verticalScale(5),
              width: isMobile
                ? responsiveScreenWidth(90)
                : responsiveScreenWidth(40),
              fontSize: scale(7),
            }}
          >
            Por {noticia.author} - {noticia.date_publish_init}
          </Text>

          <Image
            source={{
              uri: noticia.links.cover,
            }}
            style={{
              marginTop: verticalScale(10),
              width: isMobile
                ? responsiveScreenWidth(92)
                : responsiveScreenWidth(50),
              height: isMobile
                ? responsiveScreenHeight(25)
                : responsiveScreenHeight(50),
            }}
            resizeMode={isMobile ? "stretch":"center"}
          />

          <View
            style={{
              backgroundColor: "white",
              flex: 1,
              alignItems: "center",
              zIndex: 0,
              width: isMobile
                ? responsiveScreenWidth(90)
                : responsiveScreenWidth(65),
            }}
          >
            <View
              style={{
                //flex: 1,
                width: "78%",

                marginVertical: isMobile
                  ? responsiveScreenHeight(2.5)
                  : responsiveScreenHeight(2),
              }}
            >
              {Platform.OS !== "web" ? (
                <WebView
                  style={{
                    height: noticia.text.length / 5,
                    width: responsiveScreenWidth(90),
                  }}
                  textZoom={190}
                  injectedJavaScript="document.body.style.userSelect = 'none'"
                  source={{ html: noticia.text }}
                />
              ) : (
                <div dangerouslySetInnerHTML={{ __html: noticia.text }}></div>
              )}

              <BackButton onPress={() => router.push("/noticias")}></BackButton>
            </View>

            {/* <Text
              style={{
                color: Colors.green,
                textAlign: "center",
                fontWeight: "bold",
                marginVertical: isMobile
                  ? responsiveScreenHeight(2.5)
                  : responsiveScreenHeight(2.8),
                fontSize: isMobile
                  ? responsiveFontSize(2.2)
                  : responsiveFontSize(2.4),
              }}>
              O QUE AS PESSOAS TAMBÉM ESTÃO LENDO:
            </Text> */}

            <View
              style={{
                backgroundColor: "white",
                flex: 1,
                flexWrap: "wrap",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                zIndex: 1,
                gap: 5,
                width: isMobile
                  ? responsiveScreenWidth(90)
                  : responsiveScreenWidth(65),
              }}
            >
              {/* {Array.from({ length: 6 }).map((_, index) => (
              <NewsCard
                key={index}
                item={{
                  id: index,
                  description: "Mercado eleva previsão da inflação para 2024",
                  image: require("@/assets/images/noticias/mock_img.png"),
                  title: "Mercado eleva previsão da inflação para 2024",
                }}
              />
            ))} */}
            </View>
          </View>
        </PageLayout>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  image: {
    width: isMobile ? "55%" : "68%",
    height: isMobile ? 100 : responsiveScreenHeight(30),
    borderColor: "black",
    resizeMode: isMobile ? "contain" : "contain",
    marginBottom: responsiveScreenHeight(4),
  },
});
