import {
  Image,
  View,
  Dimensions,
  ScrollView,
  StatusBar,
  ImageBackground,
} from "react-native";
import { useEffect, useState } from "react";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { Text } from "@/components";
import NewsCard from "@/components/noticias/NewsCard";
import { superAudioapiApp } from "@/services/api";
import { scale, verticalScale } from "react-native-size-matters";
import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import responsiveSizes from "@/utils/responsiveSizes";
import { is } from "date-fns/locale";
import { Colors } from "@/constants/Colors";

export default function Noticias() {
  const { isMobile, RW, RH, RF } = responsiveSizes();
  const [noticias, setNoticias] = useState<any>([]);

  useEffect(() => {
    async function getNoticias() {
      try {
        const { data } = await superAudioapiApp.get(
          "news/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00"
        );
        const noticias = data.newsList.filter(
          (item: any) => item.category_multimedia.name != "Receitas"
        );
        setNoticias(noticias);
      } catch (error) {
        console.log(error);
      }
    }
    getNoticias();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        linearGradient={isMobile ?? true}
        headerTitle={
          <Image
            source={
              isMobile
                ? require("@/assets/images/noticias/logo_noticias_mob.png")
                : require("@/assets/images/noticias/logo_noticias_desk.png")
            }
            style={{
              width: isMobile ? RW(60) : RW(40),
              height: isMobile ? RH(10) : RH(10),
            }}
            resizeMode="contain"
          />
        }>
        {noticias.length > 0 && (
          <ImageBackground
            source={{
              uri: noticias[0].links.cover,
            }}
            style={{
              alignItems: "center",
              marginTop: RW(3),
              width: isMobile ? RW(95) : RW(85),
              height: isMobile ? RW(45) : RW(40),
            }}
            resizeMode="stretch">
            <View
              style={{
                width: "100%",
                height: "100%",
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center",
              }}>
              <LinearGradient
                colors={["transparent", "#000"]}
                locations={[0, 0.8]}
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  height: scale(70),
                  width: "100%",
                  alignItems: "center",
                  paddingBottom: verticalScale(15),
                }}>
                <Text
                  style={{
                    fontSize: scale(18),
                    textAlign: "center",
                    width: scale(250),
                  }}>
                  {noticias[0].headline}
                </Text>
              </LinearGradient>
            </View>
          </ImageBackground>
        )}
        <View
          style={{
            flex: 1,
            width: isMobile ? RW(90) : RW(80),
            flexWrap: "wrap",
            flexDirection: "row",
            justifyContent: "space-evenly",
            padding: 5,
            backgroundColor: "white",
            borderRadius: 5,
            //gap: 10,
            rowGap: 30,
            marginBottom: isMobile ? 30 : 80,
            paddingVertical: isMobile ? 20 : 40,
          }}>
          {noticias?.map((item: any, index: number) => (
            <NewsCard
              item={{
                id: item.id,
                title: item.headline,
                description: item.description,
                image: item.links.cover,
              }}
              key={index}
            />
          ))}
          {noticias.length == 0 && (
            <View
              style={{
                width: "100%",
                height: RH(22),
              }}>
              <Text
                fontWeight="bold"
                style={{
                  color: "black",
                  fontSize: 24,
                  textAlign: "center",
                  marginTop: 50,
                }}>
                Não há notícias
              </Text>
            </View>
          )}
        </View>
      </PageLayout>
    </ScrollView>
  );
}
