import { Link, Stack } from "expo-router";
import { StyleSheet, View } from "react-native";
import { Text } from "@/components/";

export default function NotFoundScreen() {
  return (
    <>
      <Stack.Screen
        options={{
          headerShown: false,
        }}
      />
      <View style={styles.container}>
        <Text fontWeight="bold">Essa página não existe</Text>
        <Link href="/" style={styles.link}>
          <Text>Ir para a tela inicial</Text>
        </Link>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    backgroundColor: "#0f0f0f",
  },
  link: {
    marginTop: 15,
    paddingVertical: 15,
  },
});
