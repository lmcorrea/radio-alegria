import {
  Image,
  View,
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  Pressable,
} from "react-native";
import {
  responsiveFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { BackButton, Text } from "@/components";
import { Colors } from "@/constants/Colors";
import { router, useLocalSearchParams } from "expo-router";
import { useEffect, useState } from "react";
import { authenticatedUrl, baseApiUrl } from "@/services/api";
import { loginStore } from "@/store/Login";
import { scale } from "react-native-size-matters";
import PromocoesInputHandler from "@/components/promocoes/PromocoesInputHandler";

interface Card {
  uri: string;
}

interface Branch {
  name_fantasy: string;
}

export interface PromotionDetails {
  id: number;
  type: string;
  name: string;
  question: string;
  description: string;
  rules: string;
  branch_id: number;
  company_id: number;
  show_branch_name: boolean;
  enable_cpf: boolean;
  total_participants_init: number;
  total_participants_end: number;
  enable_random_number_generation: boolean;
  card: Card;
  branch: Branch;
}

const isMobile = Dimensions.get("window").width < 768;

export default function PromocoesDetails() {
  const isMobile = Dimensions.get("window").width < 768;

  const [promotion, setPromotion] = useState<PromotionDetails>();
  const { id } = useLocalSearchParams();
  const { user } = loginStore();
  const [isParticipating, setIsParticipating] = useState(false);
  const [clickParticipate, setClickParticipate] = useState(false);

  useEffect(() => {
    async function data() {
      try {
        const { data } = await baseApiUrl.get(`controla/promotion/${id}`);
        setPromotion(data.data);
        checkIfUserIsAlreadyParticipating();
      } catch (error) {
        console.log(error);
      }
    }

    const checkIfUserIsAlreadyParticipating = async () => {
      if (!user?.token) return;

      try {
        const { data } = await authenticatedUrl.get(
          `/controla/promotion/participating?promotion_id=${id}`
        );
        setIsParticipating(data.is_participating);
        if (data?.is_participating) {
          //setParticipationNumber(data?.participation?.participationNumber);
        }
      } catch (e) {
        console.log(e);
      }
    };

    data();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        linearGradient={true}
        //whitePage={!clickParticipate}
        headerTitle={
          <>
            <Image
              source={
                isMobile
                  ? require("@/assets/images/promocoes/promocoes_logo_desk.png")
                  : require("@/assets/images/promocoes/promocoes_logo_desk.png")
              }
              style={{
                width: isMobile
                  ? responsiveScreenWidth(60)
                  : responsiveScreenWidth(25),
                height: isMobile
                  ? responsiveScreenHeight(10)
                  : responsiveScreenHeight(15),
              }}
              resizeMode="contain"
            />
            <Text
              style={{
                color: "#000",
                fontSize: scale(18),
                marginTop: 10, //scale(2),
              }}>
              PARTICIPE!
            </Text>
          </>
        }>
        <View
          style={{
            width: "100%",
            flexWrap: "wrap",
            alignItems: "center",
            gap: 10,
          }}>
          {promotion && !isParticipating && clickParticipate && (
            <View
              style={{
                alignItems: "center",
                marginVertical: isMobile ? "20%" : "5%",
                width: isMobile ? "100%" : "40%",
              }}>
              <PromocoesInputHandler
                promotion={promotion}
                setClickParticipate={setClickParticipate}
              />
            </View>
          )}

          {promotion && isParticipating && clickParticipate && (
            <View
              style={{
                paddingBottom: 50,
                alignItems: "center",
              }}>
              <Text
                style={{
                  color: "#000",
                  fontSize: responsiveFontSize(2),
                  marginTop: isMobile
                    ? responsiveScreenHeight(2)
                    : responsiveScreenHeight(3),
                  textAlign: "center",
                  marginBottom: 20,
                }}>
                Você já está participando!
              </Text>
              <BackButton
                textColor="black"
                onPress={() => setClickParticipate(false)}
              />
            </View>
          )}
          {promotion && !clickParticipate && (
            <>
              <Image
                source={{
                  uri: promotion.card.uri,
                }}
                style={styles.image}
              />

              <Text
                fontWeight="bold"
                style={{
                  color: "#000",
                  fontSize: responsiveFontSize(2),
                  marginTop: isMobile
                    ? responsiveScreenHeight(2)
                    : responsiveScreenHeight(3),
                  textAlign: "center",
                  marginBottom: 20,
                }}>
                {promotion.name}
              </Text>

              <View
                style={{
                  backgroundColor: Colors.boldGreen,
                  paddingVertical: isMobile
                    ? responsiveScreenHeight(1)
                    : responsiveScreenHeight(2),
                  paddingHorizontal: isMobile
                    ? responsiveScreenWidth(20)
                    : responsiveScreenWidth(9),
                }}>
                <Text>Participações até 30/11/2022 </Text>
              </View>

              <Text
                style={{
                  color: "#000",
                  fontSize: responsiveFontSize(1.6),
                  marginTop: isMobile
                    ? responsiveScreenHeight(2)
                    : responsiveScreenHeight(3),
                  textAlign: "left",
                  marginBottom: 20,
                }}>
                {promotion.description}
              </Text>
              {!isParticipating ? (
                <Pressable
                  onPress={() => {
                    if (user?.token) {
                      setClickParticipate(true);
                    } else {
                      router.push("/login");
                    }
                  }}>
                  <View
                    style={{
                      backgroundColor: Colors.boldGreen,
                      paddingVertical: isMobile
                        ? responsiveScreenHeight(1)
                        : responsiveScreenHeight(2),
                      paddingHorizontal: isMobile
                        ? responsiveScreenWidth(20)
                        : responsiveScreenWidth(9),
                    }}>
                    <Text>Participar</Text>
                  </View>
                </Pressable>
              ) : (
                <View>
                  <Text
                    fontWeight="bold"
                    style={{
                      color: "black",
                      marginTop: 10,
                      fontSize: isMobile
                        ? responsiveFontSize(1.6)
                        : responsiveFontSize(1.8),
                    }}>
                    Você já está participando!
                  </Text>
                </View>
              )}

              <View
                style={{
                  width: "100%",
                  marginVertical: isMobile
                    ? responsiveScreenHeight(2.5)
                    : responsiveScreenHeight(2),
                }}>
                <BackButton
                  textColor="black"
                  onPress={() => router.back()}></BackButton>
              </View>
            </>
          )}
        </View>
      </PageLayout>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  card: {
    alignItems: "center",
    margin: 10,
    marginBottom: 25,
    width: isMobile ? "100%" : responsiveScreenWidth(20),
  },

  image: {
    width: isMobile ? "90%" : "65%",
    height: isMobile ? 180 : 300,
    marginBottom: 15,
  },
});
