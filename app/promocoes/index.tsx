import { Image, View, ScrollView, StatusBar, Dimensions } from "react-native";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { Text } from "@/components";
import PromoCard from "@/components/promocoes/PromoCard";
import { useEffect, useState } from "react";
import { baseApiUrl } from "@/services/api";
import { scale, verticalScale } from "react-native-size-matters";
import { Colors } from "@/constants/Colors";
import responsiveSizes from "@/utils/responsiveSizes";

export default function Promocoes() {
  const isMobile = Dimensions.get("window").width < 768;
  const [promotions, setPromotions] = useState([]);
  const { RH, RW } = responsiveSizes();

  useEffect(() => {
    async function data() {
      try {
        const { data } = await baseApiUrl.get("controla/promotions/list/90");
        setPromotions(data.data);
      } catch (error) {
        console.log(error);
      }
    }
    data();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        headerTitle={
          <>
            <Image
              source={
                isMobile
                  ? require("@/assets/images/promocoes/promocoes_logo_desk.png")
                  : require("@/assets/images/promocoes/promocoes_logo_desk.png")
              }
              style={{
                width: scale(180),
                height: verticalScale(35),
              }}
              resizeMode="contain"
            />
            <Text
              style={{
                color: Colors.boldGreen,
                fontSize: scale(18),
                marginTop: scale(2),
              }}>
              PARTICIPE!
            </Text>
          </>
        }>
        <View
          style={{
            height: "100%",
            width: "100%",
            flexWrap: "wrap",
            flexDirection: "row",
            justifyContent: "center",
            gap: 10,
          }}>
          {promotions?.map((item: any) => (
            <PromoCard
              key={item.id}
              url="/promocoes"
              item={{
                id: item.id,
                title: item.name,
                image: item.card.uri,
              }}
            />
          ))}
          {promotions.length == 0 && (
            <View
              style={{
                height: RH(30),
              }}>
              <Text
                fontWeight="bold"
                style={{
                  color: "black",
                  fontSize: 24,
                  textAlign: "center",
                  marginTop: 20,
                }}>
                Não há promoções
              </Text>
            </View>
          )}
        </View>
      </PageLayout>
    </ScrollView>
  );
}
