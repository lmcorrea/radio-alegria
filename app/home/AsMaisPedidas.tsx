import { Text } from "@/components";
import { Colors } from "@/constants/Colors";
import { FontAwesome5, FontAwesome6 } from "@expo/vector-icons";
import { useEffect, useRef, useState } from "react";
import {
  View,
  ScrollView,
  Pressable,
} from "react-native";
import CardAsMaisPedidas from "./CardAsMaisPedidas";
import responsiveSizes from "@/utils/responsiveSizes";
import { baseApiUrl } from "@/services/api";

type AsMaisPedidasProps = {
  album: string;
  artist: string;
  created: string;
  id: number;
  img: string;
  link: string;
  modified: string;
  position: number;
  title: string;
}

const AsMaisPedidas = () => {
  const ScrollRef = useRef<ScrollView>(null);
  const ScrollWidthOffset = useRef(0);
  const [reachEnd, setReachEnd] = useState(false);
  const [reachStart, setReachStart] = useState(true);
  const { isMobile, RF, RW } = responsiveSizes();

  const [asMaisPedidas, setAsMaisPedidas] = useState<AsMaisPedidasProps[]>([]);

  useEffect(() => {
    const getAsMaisPedidas = async () => {
      try {
        const { data } = await baseApiUrl.get(
          "api/list-most-requested/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00"
        );
        setAsMaisPedidas(data.data);
      } catch (error) {
        console.log(error);
      }
    };
    getAsMaisPedidas();
  }, []);

  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 20,
      }}>
      <View
        style={{
          paddingHorizontal: isMobile ? RW(2) : RW(1.2),
          width: isMobile ? RW(100) : RW(71),
        }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: RF(2),
            paddingHorizontal: 20,
          }}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              gap: 10,
            }}>
            <FontAwesome5 name="caret-right" size={32} color={Colors.yellow} />
            <Text
              fontWeight="thin"
              style={{
                fontSize: RF(2),
              }}>
              AS MAIS PEDIDAS
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              gap: RF(2),
              justifyContent: "flex-end",
            }}>
            <Pressable
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                zIndex: 5,
                opacity: reachStart ? 0.6 : 1,
              }}
              onPress={() => {
                ScrollWidthOffset.current -= isMobile
                  ? RW(10) + 5
                  : RW(22) + 15;

                if (ScrollWidthOffset.current <= 0) {
                  ScrollWidthOffset.current = 0;
                  setReachStart(true);
                  setReachEnd(false);
                } else {
                  setReachStart(false);
                  setReachEnd(false);
                }

                ScrollRef.current?.scrollTo({
                  x: ScrollWidthOffset.current,
                  y: 0,
                  animated: true,
                });
              }}>
              <FontAwesome6
                name="arrow-left-long"
                size={isMobile ? 30 : 40}
                color={reachStart ? "white" : Colors.yellow}
              />
            </Pressable>

            <Pressable
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                zIndex: 5,
                opacity: reachEnd ? 0.6 : 1,
              }}
              onPress={() => {
                if (ScrollWidthOffset.current > asMaisPedidas.length * (isMobile ? RW(11) : RW(20))) {
                  setReachEnd(true);
                } else {
                  ScrollWidthOffset.current += isMobile
                    ? RW(10) + 5
                    : RW(22) + 15;

                  setReachEnd(false);
                  setReachStart(false);
                  ScrollRef.current?.scrollTo({
                    x: ScrollWidthOffset.current,
                    animated: true,
                  });
                }
              }}>
              <FontAwesome6
                name="arrow-right-long"
                size={isMobile ? 30 : 40}
                color={reachEnd ? "white" : Colors.yellow}
              />
            </Pressable>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "center",
            alignItems: "center",
          }}>
          <ScrollView
            ref={ScrollRef}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{
              paddingBottom: 10,
            }}
            contentContainerStyle={{
              columnGap: isMobile ? 5 : 15,
            }}>
            {asMaisPedidas?.map((item, index) => (
              <CardAsMaisPedidas
                key={index}
                index={index}
                item={{
                  color: Colors.green,
                  ...item,
                }}
              />
            ))}
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

export default AsMaisPedidas;
