import { Text } from "@/components";
import { Colors } from "@/constants/Colors";
import responsiveSizes from "@/utils/responsiveSizes";
import { Image, Linking, Pressable, View } from "react-native";

type CardAsMaisPedidasProps = {
  index: number;
  item: {
    color: string;
    album: string;
    artist: string;
    created: string;
    id: number;
    img: string;
    link: string;
    modified: string;
    position: number;
    title: string;
  };
};

const CardAsMaisPedidas = ({ index, item }: CardAsMaisPedidasProps) => {
  const { isMobile, RF, RW } = responsiveSizes();

  return (
    <Pressable
      key={index}
      onPress={() => {
        //open link
        Linking.openURL(item.link);
      }}
      style={{
        backgroundColor: item.color,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.8,
        shadowRadius: 8,
        borderRadius: 5,
        width: isMobile ? RW(31) : RW(22),
      }}>
      <Image
        style={{
          //width: isMobile ? RW(31) : RW(22),
          height: isMobile ? RF(15) : RF(19),
        }}
        source={{
          uri: item.img,
        }}
        resizeMode="cover"
      />
      <View
        style={{
          flexDirection: "row",
          //alignItems: "center",
          padding: RF(1),
          gap: 10,
          marginRight: 10,
        }}>
        <Text
          fontWeight="bold"
          style={{
            color: Colors.yellow,
            fontSize: RF(3),
          }}>
          {item.position < 10 ? `0${item.position}` : item.position}
        </Text>
        <View style={{width: "75%"}}>
          <Text
            style={{
              fontSize: RF(1.2),
              flexWrap: "wrap",
            }}>
            {item.title}
          </Text>
          <Text
            style={{
              textAlign: "left",
              flexWrap: "wrap",
              color: Colors.yellow,
              fontSize: RF(1),
            }}>
            {item.artist}
          </Text>
        </View>
      </View>
    </Pressable>
  );
};
export default CardAsMaisPedidas;
