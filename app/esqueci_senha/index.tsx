import React from "react";
import {
  View,
  Pressable,
  Alert,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  ScrollView,
  Dimensions,
} from "react-native";
import { Text } from "@/components";
import {
  responsiveFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Input from "@/components/Input";
import { LinearGradient } from "expo-linear-gradient";
import { Colors } from "@/constants/Colors";
import PageLayout from "@/components/PageLayout";
import Header from "@/components/Header";
import { Image } from "react-native";
import { router } from "expo-router";
import { loginStore } from "@/store/Login";
import { storage } from "@/services/storage";
import { scale } from "react-native-size-matters";
import { useToast } from "react-native-toast-notifications";
import { baseApiUrl } from "@/services/api";
import { companyId } from "@/constants/Tokens";

const isMobile = Dimensions.get("window").width < 768;
export default function EsqueciSenha() {
  const { handleLogIn, setUser, user } = loginStore();
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const toast = useToast();

  if (user?.token) {
    router.replace("promocoes");
    return;
  }

  async function handleLogin() {
    try {
      const { data } =  await baseApiUrl.post("radio-store/store-clients/ajax-recovery-account",{
        email,
        request_from: isMobile ? "App" : "Web",
        company_id: companyId
      });
      router.replace("login");
      
    } catch (error) {
      const message = error.response?.data?.message || "Usuário ou senha incorretos";
      toast.show(message, {
        type: "danger",
        placement: "bottom",
      });
    }
  }

  return (
    <KeyboardAvoidingView
      style={{ flex: 1, backgroundColor: "#FFF" }}
      behavior={Platform.OS === "ios" ? "padding" : "height"}>
      <ScrollView>
        <Header />
        <PageLayout
          whitePage={false}
          linearGradient={true}
          headerTitle={
            <>
              <Image
                source={
                  isMobile
                    ? require("@/assets/images/promocoes/promocoes_logo_desk.png")
                    : require("@/assets/images/promocoes/promocoes_logo_desk.png")
                }
                style={{
                  width: isMobile
                    ? responsiveScreenWidth(60)
                    : responsiveScreenWidth(25),
                  height: isMobile
                    ? responsiveScreenHeight(10)
                    : responsiveScreenHeight(15),
                }}
                resizeMode="contain"
              />
              <Text
                style={{
                  color: "#000",
                  fontSize: scale(18),
                  marginTop: isMobile
                    ? responsiveScreenHeight(2)
                    : responsiveScreenHeight(-1),
                }}>
                PARTICIPE!
              </Text>
            </>
          }>
          <LinearGradient
            colors={[Colors.lightGreen, Colors.green]}
            locations={[0.5, 0.9]}
            style={{
              width: "100%",
              alignItems: "center",
              height: "auto",
              paddingBottom: "10%",
            }}>
            <View style={styles.container}>
              <Text
                style={{
                  textAlign: "center",
                  color: Colors.yellow,
                  fontSize: responsiveFontSize(1.8),
                }}
                fontWeight="bold">
                Esqueceu sua senha?
              </Text>
              <View
                style={{
                  marginTop: "5%",
                  gap: responsiveScreenHeight(2),
                }}>
                  <Text
                    style={{
                      fontSize: responsiveFontSize(1.4),
                      marginTop: 10,
                    }}
                    fontWeight="bold">
                    Insira seu email para recuperar
                    </Text>
                <Input placeholder="Seu Email" setText={setEmail} />
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "space-between",
                    width: "100%",
                    gap: 20,
                  }}>
                  <Pressable
                    style={{
                      width: "100%",
                      height: isMobile
                        ? responsiveScreenHeight(5)
                        : responsiveScreenHeight(7),
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor: Colors.yellow,
                    }}
                    onPress={handleLogin}>
                    <Text
                      style={{
                        color: Colors.boldGreen,
                        fontSize: responsiveFontSize(1.6),
                      }}>
                      Enviar
                    </Text>
                  </Pressable>

                  <Pressable
                    style={{
                      width: "100%",
                      alignItems: "center",
                    }}
                    onPress={() => router.replace("registrar")}>
                    <Text
                      fontWeight="regular"
                      style={{
                        color: "white",
                        fontSize: responsiveFontSize(1.4),
                      }}>
                      CRIAR CONTA
                    </Text>
                  </Pressable>
                </View>
              </View>
            </View>
          </LinearGradient>
        </PageLayout>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: "5%",
    paddingHorizontal: responsiveScreenWidth(2),
    width: isMobile ? "85%" : "50%",
  },
});
