import { Platform, StatusBar, View } from "react-native";
import { useFonts } from "expo-font";
import { Stack } from "expo-router";
import * as SplashScreen from "expo-splash-screen";
import { useEffect, useRef } from "react";
import "react-native-reanimated";
import TrackPlayer, {
  AppKilledPlaybackBehavior,
  Capability,
} from "react-native-track-player";
import { emptyUser, loginStore } from "@/store/Login";
import { storage } from "@/services/storage";
import { UserType } from "@/types/RegisterServiceResponse";
import { set, setDefaultOptions } from "date-fns";
import { ptBR } from "date-fns/locale";
import { playStore } from "@/store/PlayStore";
import { Colors } from "@/constants/Colors";
import { ToastProvider } from "react-native-toast-notifications";
import { baseApiUrl } from "@/services/api";

TrackPlayer.registerPlaybackService(() => require("./services.js"));

// Prevent the splash screen from auto-hiding before asset loading is complete.
SplashScreen.preventAutoHideAsync();

export default function RootLayout() {
  const { handleRevalidateToken, setUser } = loginStore();
  const audioRef = useRef<HTMLVideoElement | null>(null);
  const { setAudioRef, setRadioInfo } = playStore();

  setDefaultOptions({ locale: ptBR });

  const setupAudioPlayer = async () => {
    try {
      await TrackPlayer.setupPlayer({});
      await TrackPlayer.updateOptions({
        alwaysPauseOnInterruption: false,
        progressUpdateEventInterval: 5, //tirar talvez
        capabilities: [
          Capability.Pause,
          Capability.Play,
          Capability.Stop,
        ],
        android: {
          appKilledPlaybackBehavior:
            AppKilledPlaybackBehavior.StopPlaybackAndRemoveNotification, //antes era   appKilledPlaybackBehavior: AppKilledPlaybackBehavior.PausePlayback,
        },
      });
      console.log("Setup is ready");
    } catch (error) {
      console.log("Setup player error at app.js");
    }
  };

  const user = storage.getString("user");

  const validateUser = async () => {
    if (user) {
      const response = await handleRevalidateToken();
      if (response.status === "success") {
        setUser(response.client);
      } else {
        storage.delete("user");
        setUser(emptyUser);
      }
    }
  };

  useEffect(() => {
    validateUser();
    setAudioRef(audioRef);
    getRadios();
  }, []);

  const [loaded] = useFonts({
    regular: require("../assets/fonts/Montserrat-Regular.ttf"),
    bold: require("../assets/fonts/Montserrat-Bold.ttf"),
    semiBold: require("../assets/fonts/Montserrat-SemiBold.ttf"),
    medium: require("../assets/fonts/Montserrat-Medium.ttf"),
    thin: require("../assets/fonts/Montserrat-Light.ttf"),
    light: require("../assets/fonts/Montserrat-Light.ttf"),
  });

  useEffect(() => {
    setupAudioPlayer();
    if (loaded) {
      SplashScreen.hideAsync();
    }
  }, [loaded]);

  if (!loaded) {
    return null;
  }

  async function getRadios() {
    try {
      const {data} = await baseApiUrl.get('controla/app-superaudio-radios/get-radios/221/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00');
      console.log("Radios", data.radios);
      setRadioInfo(data.radios);
    } catch (error) {
      console.log("Erro ao buscar radios", error);
    }
  }

  return (
    <View style={{ flex: 1, margin: 0, padding: 0, borderWidth: 0 }}>
      <ToastProvider>
        {Platform.OS === "web" && (
          <video controls ref={audioRef} hidden>
            <source type="audio/aac" />
          </video>
        )}
        <StatusBar
          barStyle="dark-content"
          backgroundColor={Colors.lightGreen}
        />
        <Stack
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="+not-found" />

          <Stack.Screen name="receitas/index" />
          <Stack.Screen name="receitas/[id]" />

          <Stack.Screen name="videos/index" />
          <Stack.Screen name="videos/[id]" />

          <Stack.Screen name="agronegocio/index" />
          <Stack.Screen name="agronegocio/[id]" />

          <Stack.Screen name="noticias/index" />
          <Stack.Screen name="noticias/[id]" />

          <Stack.Screen name="promocoes/index" />
          <Stack.Screen name="promocoes/[id]" />

          <Stack.Screen name="programacao/index" />

          <Stack.Screen name="anuncie/index" />

          <Stack.Screen name="galeria/index" />
          <Stack.Screen name="galeria/[id]" />

          <Stack.Screen name="login/index" />
          <Stack.Screen name="esqueci_senha/index" />

          <Stack.Screen name="registrar/index" />
        </Stack>
      </ToastProvider>
    </View>
  );
}
