import React, { useEffect, useState } from "react";
import { ScrollView, StatusBar, Image, View, Pressable } from "react-native";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { BackButton, Text } from "@/components/";
import { Colors } from "@/constants/Colors";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import { superAudioapi } from "@/services/api";
import { Ionicons } from "@expo/vector-icons";
import responsiveSizes from "@/utils/responsiveSizes";
import ProgramacaoItem from "@/components/programacaoItem";

export interface ProgramacaoItem {
  id: number;
  title: string;
  time_start: string;
  time_end: string;
  description: string;
  show_live_badge: boolean;
  live_video_link: string;
  cover: string;
}

type GetBanchProgramsResponse = {
  Domingo: ProgramacaoItem[];
  Segunda: ProgramacaoItem[];
  Terca: ProgramacaoItem[];
  Quarta: ProgramacaoItem[];
  Quinta: ProgramacaoItem[];
  Sexta: ProgramacaoItem[];
  Sabado: ProgramacaoItem[];
};

export default function Programacao() {
  const [isDomingo, setIsDomingo] = useState(false);
  const [showDay, setShowDay] = useState<ProgramacaoItem[]>();
  const [cityRadio, setCityRadio] = useState(221); //221 pelotas - 374 novo hamburgo
  const { isMobile, RH, RW, RF } = responsiveSizes();

  const handleGetProgramacao = async () => {
    const { data } = await superAudioapi.get(
      `get-branch-programs/${cityRadio}/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00` // pegar o id de novo hamburgo e pelotas
    );
    const programacaoResponse: GetBanchProgramsResponse = data.programs;
    isDomingo
      ? setShowDay(programacaoResponse?.Domingo)
      : setShowDay(programacaoResponse?.Segunda);
  };

  useEffect(() => {
    handleGetProgramacao();
  }, [isDomingo, cityRadio]);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        headerTitle={
          <Image
            source={
              isMobile
                ? require("@/assets/images/programacao.png")
                : require("@/assets/images/programacao.png")
            }
            style={{
              width: isMobile ? RW(60) : RW(40),
              height: isMobile ? RH(10) : RH(10),
            }}
            resizeMode="contain"
          />
        }>
        <View
          style={{
            marginTop: "5%",
            gap: RF(3),
          }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              gap: 20,
              paddingHorizontal: RW(10),
            }}>
            <Pressable
              style={{
                backgroundColor: "white",
                padding: 20,
                borderRadius: 10,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                width: isMobile ? RW(40) : RW(20),
              }}
              onPress={() => setCityRadio(374)}>
              <Text
                fontWeight="bold"
                style={{
                  textAlign: "center",
                  fontSize: RF(1.3),
                  color: cityRadio === 374 ? Colors.yellow : Colors.boldGreen,
                }}>
                Região Metropolitana
              </Text>
            </Pressable>
            <Pressable
              style={{
                backgroundColor: "white",
                padding: 20,
                borderRadius: 10,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                width: isMobile ? RW(40) : RW(20),
              }}
              onPress={() => setCityRadio(221)}>
              <Text
                fontWeight="bold"
                style={{
                  textAlign: "center",
                  fontSize: RF(1.3),
                  color: cityRadio === 221 ? Colors.yellow : Colors.boldGreen,
                }}>
                Região Sul
              </Text>
            </Pressable>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              gap: 6,
            }}>
            <Pressable
              onPress={() => setIsDomingo(false)}
              style={{
                backgroundColor: Colors.boldGreen,
                borderWidth: 0.3,
                borderColor: "white",
                padding: 10,
                borderRadius: 10,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                width: isMobile ? RW(28) : RW(20),
              }}>
              <Text
                fontWeight="bold"
                style={{
                  textAlign: "center",
                  fontSize: RF(1.2),
                  color: isDomingo ? "white" : Colors.yellow,
                }}>
                SEG A. SAB.
              </Text>
            </Pressable>
            <Pressable
              onPress={() => setIsDomingo(true)}
              style={{
                backgroundColor: Colors.boldGreen,
                borderWidth: 0.3,
                borderColor: "white",
                padding: 10,
                borderRadius: 10,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                width: isMobile ? RW(28) : RW(20),
              }}>
              <Text
                fontWeight="bold"
                style={{
                  textAlign: "center",
                  fontSize: RF(1.2),
                  color: isDomingo ? Colors.yellow : "white",
                }}>
                DOMINGO
              </Text>
            </Pressable>
          </View>
        </View>
        <View
          style={{
            //backgroundColor: Colors.lightGreen,
            width: isMobile ? "100%" : "75%",
            paddingVertical: 30,
            gap: RF(1.2),
            paddingHorizontal: "5%",
          }}>
          {showDay?.map((item, index) => (
            <ProgramacaoItem key={index} item={item} />
          ))}
        </View>
        <View
          style={{
            paddingBottom: 100,
          }}>
          <BackButton textColor="white" />
        </View>
      </PageLayout>
    </ScrollView>
  );
}
