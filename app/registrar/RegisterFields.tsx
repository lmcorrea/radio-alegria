import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  Pressable,
  Platform,
  Alert,
  Dimensions,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Input from "../../components/Input";
import SelectDropdown from "react-native-select-dropdown";
import axios from "axios";
import { Colors } from "@/constants/Colors";
import PulsingDots from "@/components/PulsingDots";
import { loginStore } from "@/store/Login";
import { storage } from "@/services/storage";
import { router } from "expo-router";
import { moderateScale, scale } from "react-native-size-matters";
import { useToast } from "react-native-toast-notifications";

export default function RegisterFields() {
  const [stateList, setStateList] = useState([]);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [city, setCity] = useState("");
  const [phone, setPhone] = useState("");
  const [state, setState] = useState("");
  const [cityList, setCityList] = useState([]);
  const [loading, setLoading] = useState(false);
  const toast = useToast();

  const isMobile = Dimensions.get("window").width < 768;

  const { handleRegisterUser, setUser } = loginStore();
  const handleRegister = async () => {
    setLoading(true);
    try {
      let phone_fitered = phone.replace(/\D/g, "");

      const data = await handleRegisterUser({
        email,
        password,
        name,
        phone: phone_fitered,
        state,
        city,
        requestFrom: isMobile ? "App" : "Web",
      });

      // storage.set("user", JSON.stringify(data.client));
      // setUser(data.client);
      // isMobile && Alert.alert("Cadastrado com sucesso!");
      toast.show("Cadastrado com sucesso!", {
        type: "success",
        placement: "bottom",
      });
      
      router.replace("promocoes");
    } catch (e: any) {
      console.log(e);
      toast.show(e?.response?.data?.message || 'Erro ao cadastrar' , {
        type: "danger",
        placement: "bottom",
      });
    } finally {
      setLoading(false);
    }
  };

  const handleGetEstados = async () => {
    const { data } = await axios.get(
      "https://api.superaudio.com.br/api/address/request-states/884d9804999fc47a3c2694e49ad2536a"
    );
    setStateList(data.data);
  };

  const handleGetCidades = async (estadoUf: any) => {
    const { data } = await axios.get(
      "https://api.superaudio.com.br/api/address/request-cities/884d9804999fc47a3c2694e49ad2536a/" +
        estadoUf
    );
    setCityList(data.data);
  };

  const handleChangeTel = (event: any) => {
    let x = event.replace(/\D/g, "").match(/(\d{0,2})(\d{0,5})(\d{0,4})/);
    event = !x[2] ? x[1] : "(" + x[1] + ") " + x[2] + (x[3] ? "-" + x[3] : "");
    setPhone(event);
  };

  useEffect(() => {
    handleGetEstados();
  }, []);

  useEffect(() => {
    if (state) {
      handleGetCidades(state);
    }
  }, [state]);

  const fieldsNotFilled =
    !email || !password || !name || !phone || !state || !city;

  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      //behavior={Platform.OS === "ios" ? "padding" : null}
    >
      <ScrollView>
        <View style={styles.container}>
          <View style={{ marginTop: "10%", gap: responsiveScreenHeight(2) }}>
            <Input placeholder="Nome Completo*" setText={setName} />

            <Input placeholder="E-mail*" setText={setEmail} />

            <Input placeholder="Senha*" setText={setPassword}  InputProps={{
              secureTextEntry: true
            }} />

            <Input
              type="numeric"
              placeholder="Telefone*"
              setText={handleChangeTel}
              value={phone}
            />

            <SelectDropdown
              data={stateList}
              onSelect={(selectedItem) => {
                setState(selectedItem.uf);
              }}
              buttonTextAfterSelection={(selectedItem: any) =>
                selectedItem.name
              }
              rowTextForSelection={(item: any) => item.name}
              rowStyle={{
                width: "100%",
                backgroundColor: "#fff",
              }}
              dropdownStyle={{
                backgroundColor: "#fff",
                borderRadius: 10,
              }}
              buttonStyle={{
                width: "100%",
                backgroundColor: "#fff",
                borderRadius: isMobile ? 20 : 50,
                borderColor: "#fff",
              }}
              renderCustomizedButtonChild={(selectedItem: any) => {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      borderRadius: 10,
                      padding: 7,
                    }}>
                    <Text
                      style={{
                        fontSize: responsiveScreenFontSize(1),
                        color: "#2d2d2d",
                      }}>
                      {selectedItem ? selectedItem.name : "Estado*"}
                    </Text>
                  </View>
                );
              }}
            />
            {state && (
              <SelectDropdown
                disabled={!state}
                data={cityList}
                onSelect={(selectedItem) => {
                  setCity(selectedItem.id);
                }}
                buttonTextAfterSelection={(selectedItem: any) =>
                  selectedItem.name
                }
                rowTextForSelection={(item: any) => item.name}
                rowStyle={{
                  width: "100%",
                  backgroundColor: "#fff",
                }}
                dropdownStyle={{
                  backgroundColor: "#fff",
                  borderRadius: 10,
                }}
                buttonStyle={{
                  width: "100%",
                  backgroundColor: "#fff",
                  borderRadius: isMobile ? 20 : 50,
                  //borderWidth: 1,
                  borderColor: "#fff",
                }}
                search
                searchInputStyle={{
                  borderColor: "#fff",
                  backgroundColor: "#fff",
                  width: "95%",
                  borderRadius: 10,
                  marginHorizontal: 10,
                  marginTop: 5,
                }}
                searchPlaceHolder="Cidade*"
                renderCustomizedButtonChild={(selectedItem: any) => {
                  return (
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                      }}>
                      <Text
                        style={{
                          fontSize: responsiveScreenFontSize(1),
                          color: "#2d2d2d",
                        }}>
                        {selectedItem ? selectedItem.name : "Cidade*"}
                      </Text>
                    </View>
                  );
                }}
              />
            )}
          </View>

          <Pressable
            disabled={loading || fieldsNotFilled}
            style={{
              marginTop: "10%",
              borderRadius: 10,
              backgroundColor: fieldsNotFilled ? "#999999" : Colors.red,
              height: responsiveScreenHeight(7),
              alignItems: "center",
              justifyContent: "center",
            }}
            onPress={handleRegister}>
            {loading ? (
              <PulsingDots />
            ) : (
              <Text
                style={{
                  color: "#fff",
                  fontSize: responsiveScreenFontSize(1),
                  fontWeight: "bold",
                }}>
                CADASTRAR
              </Text>
            )}
          </Pressable>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: "10%",
    paddingHorizontal: responsiveScreenWidth(2),
    marginBottom: 30,
    width: "100%",
  },
});
