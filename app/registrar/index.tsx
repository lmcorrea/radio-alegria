import React from "react";
import { Text, StatusBar, Image, Dimensions, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import PageLayout from "@/components/PageLayout";
import { Header } from "@/components";
import RegisterFields from "./RegisterFields";
import { scale } from "react-native-size-matters";

export default function RegistreSe() {
  const isMobile = Dimensions.get("window").width < 768;

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        whitePage={false}
        headerTitle={
          <>
            <Image
              source={
                isMobile
                  ? require("@/assets/images/promocoes/promocoes_logo_desk.png")
                  : require("@/assets/images/promocoes/promocoes_logo_desk.png")
              }
              style={{
                width: isMobile
                  ? responsiveScreenWidth(60)
                  : responsiveScreenWidth(25),
                height: isMobile
                  ? responsiveScreenHeight(5)
                  : responsiveScreenHeight(15),
              }}
              resizeMode="contain"
            />
            <Text
              style={{
                color: "#000",
                fontSize: scale(18),
                marginTop: isMobile
                  ? responsiveScreenHeight(2)
                  : responsiveScreenHeight(-1),
              }}>
              PARTICIPE!
            </Text>
          </>
        }>
        <View style={{
          width: isMobile ? "100%" : "70%",
        }}>
          <RegisterFields />
        </View>
      </PageLayout>
    </ScrollView>
  );
}
