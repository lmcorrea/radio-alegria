import {
  Image,
  Platform,
  View,
  ImageBackground,
  Dimensions,
  ScrollView,
  StatusBar,
  PixelRatio,
  TextInput,
} from "react-native";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { Card, Text } from "@/components";
import { useEffect, useState } from "react";
import { superAudioapiApp } from "@/services/api";
import { CardType } from "@/components/Card";
import Input from "@/components/Input";
import Icon from "react-native-vector-icons/FontAwesome";
import { scale } from "react-native-size-matters";
import responsiveSizes from "@/utils/responsiveSizes";
import React from "react";
import { Colors } from "@/constants/Colors";

export default function Receitas() {
  const { RF, isMobile, RW, RH } = responsiveSizes();

  const [receitas, setReceitas] = useState<CardType[]>([]);
  const [filterInput, setFilterInput] = useState("");

  useEffect(() => {
    async function getNoticias() {
      try {
        const { data } = await superAudioapiApp.get(
          "news/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00"
        );
        const noticias = data.newsList.filter(
          (item: any) => item.category_multimedia.name == "Receitas"
        );
        setReceitas(noticias);
      } catch (error) {
        console.log(error);
      }
    }
    getNoticias();
  }, []);

  const receitasToShow = filterInput
    ? receitas.filter((receita) =>
        receita.headline.toLowerCase().includes(filterInput.toLowerCase())
      )
    : receitas;

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        linearGradient={isMobile ?? true}
        headerTitle={
          <Image
            source={require("@/assets/images/receitas/receita_logo_desk.png")}
            style={{
              width: isMobile ? RW(60) : RW(25),
              marginTop: RW(0),
            }}
            resizeMode="contain"
          />
        }>
        <View
          style={{
            marginBottom: 30,
            width: scale(200),
          }}>
          <Input
            icon={<Icon name="search" size={20} color="gray" />}
            setText={setFilterInput}
            value={filterInput}
            placeholder="Pesquise sua receita"
          />
        </View>
        <View
          style={{
            width: "100%",
            marginBottom: 50,
            //gap: RF(3),
          }}>
          {receitas &&
            receitasToShow.length > 0 &&
            receitasToShow.map((receita: CardType, Index) => (
              <View
                key={receita.id}
                style={{
                  paddingBottom: RF(2),
                  paddingTop: RF(2),
                  borderBottomColor: Colors.green,
                  borderBottomWidth:
                    receitasToShow && Index !== receitasToShow.length - 1
                      ? 1
                      : 0,
                }}>
                <Card item={receita} url="receitas" />
              </View>
            ))}
        </View>

        {receitasToShow.length === 0 && (
          <View
            style={{
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20,
            }}>
            <Text
              style={{
                color: "black",
                fontSize: 20,
                fontWeight: "bold",
              }}>
              Nenhuma receita encontrada
            </Text>
          </View>
        )}
      </PageLayout>
    </ScrollView>
  );
}
