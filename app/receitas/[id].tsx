import { router, useLocalSearchParams } from "expo-router";

import {
  Image,
  Platform,
  View,
  ScrollView,
  StatusBar,
  StyleSheet,
  Dimensions,
} from "react-native";
import {
  responsiveFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Header from "@/components/Header";
import PageLayout from "@/components/PageLayout";
import { BackButton, Text } from "@/components";
import { Colors } from "@/constants/Colors";
import { useEffect, useState } from "react";
import { superAudioapiApp } from "@/services/api";
import HtmlContent from "@/components/HtmlContent";
import WebView from "react-native-webview";
import { scale, verticalScale } from "react-native-size-matters";

const isMobile = Dimensions.get("window").width < 768;

type ReceitaDetails = {
  headline: string;
  description: string;
  text: string;
  date_publish_init: string;
  highlight: number;
  author: string;
  category_multimedia: {
    name: string;
  };
  links: {
    cover: string;
  };
  videos: any[]; // Consider specifying a more detailed type if the structure of videos is known
  audios: any[]; // Consider specifying a more detailed type if the structure of audios is known
};

export default function Receita() {
  const { id } = useLocalSearchParams();
  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState<ReceitaDetails>();

  useEffect(() => {
    async function getDetails() {
      setLoading(true);
      try {
        const { data } = await superAudioapiApp.get(
          `news/${id}/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00/`
        );
        setItem(data.news);
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    }
    getDetails();
  }, []);

  return (
    <ScrollView style={{ paddingTop: StatusBar.currentHeight }}>
      <Header />
      <PageLayout
        headerTitle={
          <Image
            source={
              isMobile
                ? require("@/assets/images/receitas/receita_logo_mob.png")
                : require("@/assets/images/receitas/receita_logo_desk.png")
            }
            style={{
              width: isMobile
                ? responsiveScreenWidth(60)
                : responsiveScreenWidth(40),
              height: isMobile
                ? responsiveScreenHeight(5)
                : responsiveScreenHeight(10),
            }}
            resizeMode="contain"
          />
        }>
        {!loading && (
          <View
            style={{
              width: "100%",
            }}>
            <Text
              style={{
                marginBottom: responsiveScreenHeight(4),
                color: Colors.boldGreen,
                textAlign: "center",
                fontWeight: "bold",
                fontSize: isMobile
                  ? responsiveFontSize(2.2)
                  : responsiveFontSize(2.5),
              }}>
              {item?.headline}
            </Text>
            <Image source={{ uri: item?.links.cover }} style={styles.image} />
            <View style={{
              alignItems: "center",
              marginVertical: 10,
            }}>
              <Text
                style={{
                  color: Colors.boldGreen,
                  fontWeight: "bold",
                  textAlign: "center",
                  fontSize: isMobile
                    ? responsiveFontSize(2.2)
                    : responsiveFontSize(2.2),
                }}>
                Ingredientes
              </Text>
              <Text
                style={{
                  marginTop: responsiveScreenHeight(2),
                  color: "#000",
                  //fontWeight: "bold",
                  fontSize: isMobile
                    ? responsiveFontSize(1.6)
                    : responsiveFontSize(1.3),
                  marginBottom: responsiveScreenHeight(2),
                }}>
                {item?.description}
              </Text>
            </View>
            <View
              style={{
                marginTop: responsiveScreenHeight(2),
                //width: "110%",
                backgroundColor: Colors.gray,
                marginHorizontal: -10,
                paddingVertical: verticalScale(15),
              }}>
              <Text
                style={{
                  color: "#000",
                  fontWeight: "bold",
                  paddingLeft: 20,
                  fontSize: isMobile
                    ? responsiveFontSize(2.2)
                    : responsiveFontSize(1.2),
                }}>
                Modo de preparo:
              </Text>
              {Platform.OS != "web" ? (
                <WebView
                  style={{
                    height: item.text.length / 3,
                    //top: -2,
                    backgroundColor: Colors.gray,
                    width: "100%",
                  }}
                  textZoom={200}
                  injectedJavaScript="document.body.style.userSelect = 'none'"
                  source={{
                    html: `
                    <head>
                      <link rel="preconnect" href="https://fonts.googleapis.com">
                      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
                      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
                      <style>
                        body {
                          font-family: 'Montserrat', sans-serif;
                          background-color: ${Colors.gray};
                        }
                        .montserrat-<uniquifier> {
                          font-family: "Montserrat", sans-serif;
                          font-optical-sizing: auto;
                          font-weight: <weight>;
                          font-style: normal;
                        }
                      </style>
                    </head>
                    <body>
                      <div style="background-color:${Colors.gray}; padding: 50px">
                      ${item.text}
                      </div>
                    </body>
                    `,
                  }}
                />
              ) : (
                <HtmlContent
                  html={item?.text as string}
                  styles={{
                    paddingInline: 20,
                    fontSize: isMobile
                      ? responsiveFontSize(1.6)
                      : responsiveFontSize(1.3),
                    fontFamily: "regular",
                  }}
                />
              )}
            </View>

            <View
              style={{
                width: "100%",
                marginTop: isMobile
                  ? responsiveScreenHeight(2)
                  : responsiveScreenHeight(4),
              }}>
              <BackButton
                onPress={() => router.navigate("/receitas")}></BackButton>
            </View>
          </View>
        )}
      </PageLayout>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  image: {
    width: isMobile ? "55%" : "68%",
    height: isMobile ? 100 : responsiveScreenHeight(30),
    resizeMode: isMobile ? "contain" : "contain",
    alignSelf: "center",
    marginBottom: responsiveScreenHeight(4),
  },
});
