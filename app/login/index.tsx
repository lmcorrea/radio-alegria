import React from "react";
import {
  View,
  Pressable,
  Alert,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  ScrollView,
  Dimensions,
} from "react-native";
import { Text } from "@/components";
import {
  responsiveFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import Input from "@/components/Input";
import { LinearGradient } from "expo-linear-gradient";
import { Colors } from "@/constants/Colors";
import PageLayout from "@/components/PageLayout";
import Header from "@/components/Header";
import { Image } from "react-native";
import { router } from "expo-router";
import { loginStore } from "@/store/Login";
import { storage } from "@/services/storage";
import { scale } from "react-native-size-matters";
import { useToast } from "react-native-toast-notifications";

const isMobile = Dimensions.get("window").width < 768;
export default function login() {
  const { handleLogIn, setUser, user } = loginStore();
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const toast = useToast();

  if (user?.token) {
    router.replace("promocoes");
    return;
  }

  async function handleLogin() {
    try {
      console.log("aaaaaaa");
      const response = await handleLogIn(
        email,
        password,
        isMobile ? "App" : "Web"
      );
      setUser(response.client);
      storage.set("user", JSON.stringify(response.client));
      router.replace("promocoes");
      
    } catch (error) {
      const message = error.response?.data?.message || "Usuário ou senha incorretos";
      toast.show(message, {
        type: "danger",
        placement: "bottom",
      });
    }
  }

  return (
    <KeyboardAvoidingView
      style={{ flex: 1, backgroundColor: "#FFF" }}
      behavior={Platform.OS === "ios" ? "padding" : "height"}>
      <ScrollView>
        <Header />
        <PageLayout
          whitePage={false}
          linearGradient={true}
          headerTitle={
            <>
              <Image
                source={
                  isMobile
                    ? require("@/assets/images/promocoes/promocoes_logo_desk.png")
                    : require("@/assets/images/promocoes/promocoes_logo_desk.png")
                }
                style={{
                  width: isMobile
                    ? responsiveScreenWidth(60)
                    : responsiveScreenWidth(25),
                  height: isMobile
                    ? responsiveScreenHeight(10)
                    : responsiveScreenHeight(15),
                }}
                resizeMode="contain"
              />
              <Text
                style={{
                  color: "#000",
                  fontSize: scale(18),
                  marginTop: isMobile
                    ? responsiveScreenHeight(2)
                    : responsiveScreenHeight(-1),
                }}>
                PARTICIPE!
              </Text>
            </>
          }>
          <LinearGradient
            colors={[Colors.lightGreen, Colors.green]}
            locations={[0.5, 0.9]}
            style={{
              width: "100%",
              alignItems: "center",
              height: "auto",
              paddingBottom: "10%",
            }}>
            <View style={styles.container}>
              <Text
                style={{
                  textAlign: "center",
                  color: Colors.yellow,
                  fontSize: responsiveFontSize(1.8),
                }}
                fontWeight="bold">
                Faça seu login para participar
              </Text>
              <View
                style={{
                  marginTop: "10%",
                  gap: responsiveScreenHeight(2),
                }}>
                <Input placeholder="Seu Email" setText={setEmail} />
                <Input password placeholder="Sua Senha" setText={setPassword} />
                <Text
                  onPress={() => router.navigate("esqueci_senha")}
                  style={{
                    color: Colors.yellow,
                    textAlign: "right",
                    marginBottom: 30,
                  }}>
                  Esqueci Minha Senha
                </Text>
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "space-between",
                    width: "100%",
                    gap: 20,
                  }}>
                  <Pressable
                    style={{
                      width: "100%",
                      height: isMobile
                        ? responsiveScreenHeight(5)
                        : responsiveScreenHeight(7),
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor: Colors.yellow,
                    }}
                    onPress={handleLogin}>
                    <Text
                      style={{
                        color: Colors.boldGreen,
                        fontSize: responsiveFontSize(1.6),
                      }}>
                      ENTRAR
                    </Text>
                  </Pressable>

                  <Pressable
                    style={{
                      width: "100%",
                      alignItems: "center",
                    }}
                    onPress={() => router.replace("registrar")}>
                    <Text
                      fontWeight="regular"
                      style={{
                        color: "white",
                        fontSize: responsiveFontSize(1.4),
                      }}>
                      CRIAR CONTA
                    </Text>
                  </Pressable>
                </View>
              </View>
            </View>
          </LinearGradient>
        </PageLayout>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: "5%",
    paddingHorizontal: responsiveScreenWidth(2),
    width: isMobile ? "85%" : "50%",
  },
});
