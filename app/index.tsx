import {
  Image,
  View,
  Pressable,
  ImageBackground,
  Dimensions,
  ScrollView,
  StatusBar,
  Linking,
} from "react-native";
import { Header, Text } from "@/components/";
import { Colors } from "@/constants/Colors";
import { FontAwesome, FontAwesome5, FontAwesome6 } from "@expo/vector-icons";
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { scale } from "react-native-size-matters";
import { Marquee } from "@animatereactnative/marquee";

import { SwiperFlatList } from "react-native-swiper-flatlist";
import { router } from "expo-router";
import PolaroidImages, {
  MobilePolaroidImages,
} from "@/components/home/PolaroidImages";
import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect, useState } from "react";
import { superAudioapi, superAudioapiApp } from "@/services/api";
import NewsCard from "@/components/noticias/NewsCard";
import { addDays, format, isWithinInterval, parse } from "date-fns";
import AsMaisPedidas from "./home/AsMaisPedidas";
import responsiveSizes from "@/utils/responsiveSizes";
import ProgramacaoItem from "@/components/programacaoItem";

export interface ProgramacaoItem {
  id: number;
  title: string;
  time_start: string;
  time_end: string;
  description: string;
  show_live_badge: boolean;
  live_video_link: string;
  cover: string;
}

type GetBanchProgramsResponse = {
  Domingo: ProgramacaoItem[];
  Segunda: ProgramacaoItem[];
  Terca: ProgramacaoItem[];
  Quarta: ProgramacaoItem[];
  Quinta: ProgramacaoItem[];
  Sexta: ProgramacaoItem[];
  Sabado: ProgramacaoItem[];
};

export default function HomeScreen() {
  const [noticias, setNoticias] = useState<any>([]);
  const [programacao, setProgramacao] = useState<ProgramacaoItem[]>();
  const [banners, setBanners] = useState<any>([]);

  const { isMobile, RF, RH, RW } = responsiveSizes();

  const footerLocations = [
    {
      cidade: "NOVO HAMBURGO/RS",
      rua: "Rua Araribóia, 38",
      bairro: "Rio Branco",
      cep: "93310-090",
      telefone: "51 3594.6222",
    },
    {
      cidade: "PORTO ALEGRE/RS",
      rua: "Av Praia de Belas, 1212",
      bairro: "Praia de Belas",
      cep: "90110-000",
      telefone: "51 3594.6222",
    },
    {
      cidade: "PELOTAS/RS",
      rua: "Av. Dom Joaquim, 1515",
      bairro: "Sala 709 | Centro",
      cep: "96020-260",
      telefone: "51 3227.0211",
    },
    {
      cidade: "BRASÍLIA/DF",
      rua: "Versus",
      bairro: "Representações",
      cep: "",
      telefone: "61 3221.9100",
    },
  ];

  const [windowDimensions, setWindowDimensions] = useState(
    Dimensions.get("window")
  );

  useEffect(() => {
    const handleResize = () => {
      setWindowDimensions(Dimensions.get("window"));
    };

    Dimensions.addEventListener("change", handleResize);
  }, []);

  useEffect(() => {
    getNoticias();
    handleGetProgramacao();
    getBanners();
  }, []);

  async function getBanners() {
    try {
      const { data } = await superAudioapiApp.get(
        "banners/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00"
      );
      setBanners(data.data);
    } catch (error) {
      console.log(error);
    }
  }

  async function getNoticias() {
    try {
      const { data } = await superAudioapiApp.get(
        "news/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00"
      );
      const noticias = data.newsList.filter(
        (item: any) => item.category_multimedia.name != "Receitas"
      );
      //setNoticias(noticias.slice(0, 3));
      setNoticias(noticias);
    } catch (error) {
      console.log(error);
    }
  }

  const FormatProgramacao = (programacao: any): any => {
    const hoje = new Date();
    const diaSemana = (
      format(hoje, "EEEE").charAt(0).toUpperCase() +
      format(hoje, "EEEE").slice(1).toLowerCase()
    ).split("-")[0];

    const programasHoje = programacao[diaSemana];

    const horaAtual = format(hoje, "HH:mm");
    const programaAtual = programasHoje.find((programa: ProgramacaoItem) => {
      const horaInicio = parse(programa.time_start, "HH:mm", new Date());

      let horaFim = parse(programa.time_end, "HH:mm", new Date());
      if (horaFim < horaInicio) {
        horaFim = addDays(horaFim, 1);
      }

      const intervalo = { start: horaInicio, end: horaFim };
      const horaAtualDate = parse(horaAtual, "HH:mm", hoje);
      return isWithinInterval(horaAtualDate, intervalo);
    });

    return programaAtual;
  };

  const handleGetProgramacao = async () => {
    try {
      const { data: pelotas } = await superAudioapi.get(
        `get-branch-programs/221/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00` // pegar o id de novo hamburgo e pelotas
      );

      const { data: novoHamburgo } = await superAudioapi.get(
        `get-branch-programs/374/AD65BD94017A866C8A3B3ED7ACB6AEF8186A9F00` // pegar o id de novo hamburgo e pelotas
      );

      const programaPelotas = FormatProgramacao(pelotas.programs);
      const programaNovoHamburgo = FormatProgramacao(novoHamburgo.programs);
      let programacao = [programaPelotas, programaNovoHamburgo].filter(
        (programa) => programa
      );
      if (programaPelotas?.title === programaNovoHamburgo?.title) {
        programacao.pop();
      }

      setProgramacao(programacao);
    } catch (error) {
      console.log("error", error);
    }
  };

  return (
    <ScrollView style={{ marginTop: StatusBar.currentHeight }}>
      <Header />
      <View>
        <View>
          <SwiperFlatList
            autoplay
            autoplayLoopKeepAnimation={true}
            showPagination
            data={banners}
            renderItem={({ item }) => (
              <Pressable
                onPress={() => {
                  if (item.link != "" && item.link) Linking.openURL(item.link);
                }}>
                <Image
                  source={{
                    uri: item.dir,
                  }}
                  resizeMode="cover"
                  style={{
                    width: responsiveScreenWidth(100),
                    height: isMobile
                      ? responsiveScreenHeight(20)
                      : responsiveScreenHeight(50),
                  }}
                />
              </Pressable>
            )}
            pagingEnabled
            paginationDefaultColor="transparent"
            paginationStyleItem={{
              width: scale(10),
              height: scale(10),
              borderRadius: 20,
              marginHorizontal: 7,
              borderWidth: 2,
              borderColor: "#FFF",
            }}
            paginationStyle={{
              alignSelf: "flex-end",
              right: scale(20),
            }}
          />
        </View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginLeft: "7%",
            marginVertical: "3%",
          }}>
          <FontAwesome5 name="caret-right" size={32} color={Colors.yellow} />
          <Text
            fontWeight="thin"
            style={{
              fontSize: RF(2.2),
              color: Colors.green,
            }}>
            NO AR AGORA
          </Text>
        </View>

        <View
          style={{
            backgroundColor: Colors.lightGreen,
            paddingVertical: 30,
            gap: RF(2),
            paddingBottom: isMobile ? 30 : 60,
            paddingHorizontal: isMobile ? "5%" : "10%",
          }}>
          {programacao?.map((item, index) => (
            <ProgramacaoItem item={item} key={index} />
          ))}
        </View>

        <ImageBackground
          source={require("@/assets/images/background.png")}
          //resizeMode="repeat"
          resizeMethod="resize"
          style={{
            height: "auto",
            width: "100%",
            //gap: isMobile ? 20 : 40,
          }}>
          {/* NOTICIAS DO AGRO */}
          <Marquee
            speed={1}
            spacing={10}
            style={{
              backgroundColor: Colors.boldGreen,
              paddingVertical: isMobile ? 5 : 10,
            }}>
            <Pressable
              onPress={() => router.navigate("noticias")}
              style={{
                width: "100%",
                gap: 5,
                flexDirection: "row",
              }}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <FontAwesome5
                  name="caret-right"
                  size={32}
                  color={Colors.yellow}
                />
                <Text
                  fontWeight="thin"
                  style={{
                    fontSize: responsiveScreenFontSize(2),
                  }}>
                  NOTÍCIAS DO AGRO
                </Text>
              </View>
            </Pressable>
          </Marquee>
          {/* NOTICIAS */}
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "center",
              gap: 10,
              alignItems: "center",
              backgroundColor: Colors.mediumGreen,
              paddingVertical: 15,
            }}>
            <View
              style={{
                opacity: 0.6,
                width: "100%",
                //height: "100%", //ALTERAR
                position: "absolute",
              }}
            />
            {noticias?.slice(0, 9).map((item: any, index: number) => (
              <NewsCard
                item={{
                  id: item.id,
                  title: item.headline,
                  description: item.description,
                  image: item.links.cover,
                }}
                key={index}
              />
            ))}
          </View>
          {isMobile ? (
            <LinearGradient
              colors={[Colors.lightGreen, "transparent"]}
              locations={[0.5, 0.9]}>
              <AsMaisPedidas />
              <MobilePolaroidImages />
            </LinearGradient>
          ) : (
            <>
              <AsMaisPedidas />
              <PolaroidImages />
            </>
          )}

          <View>
            <LinearGradient
              style={{
                width: "100%",
                height: "100%", //ALTERAR
                position: "absolute",
              }}
              locations={[0.0, 0.9]}
              colors={["transparent", Colors.green]}
            />
            <View
              style={{
                marginTop: 30,
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
                flexWrap: "wrap",
                gap: 20,
              }}>
              <Pressable
                onPress={() => router.navigate("agronegocio")}
                style={{
                  alignItems: "center",
                }}>
                <Image
                  source={require("@/assets/images/agronegocio.png")}
                  style={{
                    width: RW(43),
                    height: RW(16),
                  }}
                />
              </Pressable>
              <Pressable
                onPress={() => router.navigate("/anuncie")}
                style={{
                  alignItems: "center",
                }}>
                <Image
                  source={require("@/assets/images/afiliase.png")}
                  style={{
                    width: RW(43),
                    height: RW(16),
                  }}
                />
              </Pressable>
            </View>
            {/* FOOTER */}

            <View
              style={{
                flexDirection: "row",
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center",
                marginTop: 30,
                paddingBottom: 50,
                width: "100%",
                rowGap: RF(2),
                //borderWidth: 2,
              }}>
              {footerLocations.map((item, index) => (
                <View
                  key={index}
                  style={{
                    borderRightWidth: index === 4 ? 0 : 3, // LINHA AMARELA
                    borderTopEndRadius: 2,
                    borderBottomEndRadius: 2,
                    borderColor: Colors.yellow,
                    paddingRight: RW(2),
                    paddingLeft: RW(2),
                  }}>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      height: RF(7),
                      width: "100%",
                      gap: isMobile ? 5 : RF(1),
                    }}>
                    {/* ICONES */}
                    <View
                      style={{
                        justifyContent: "space-between",
                        height: "100%",
                        paddingVertical: isMobile ? 0 : 5,
                      }}>
                      <FontAwesome6
                        name="location-dot"
                        size={RF(1.5)}
                        color={Colors.yellow}
                      />
                      <FontAwesome
                        name="phone-square"
                        size={RF(1.5)}
                        color={Colors.yellow}
                      />
                    </View>
                    {/* TEXTOS */}
                    <View style={{ gap: isMobile ? RF(0.5) : RF(0.5) }}>
                      <Text
                        fontWeight="bold"
                        style={{
                          fontSize: RF(0.9),
                        }}>
                        {item.cidade}
                      </Text>
                      <Text
                        style={{
                          fontSize: RF(0.9),
                        }}>
                        {item.rua}
                      </Text>
                      <Text
                        style={{
                          fontSize: RF(0.9),
                        }}>
                        {item.bairro}
                      </Text>
                      <Text
                        style={{
                          fontSize: RF(0.8),
                        }}>
                        {/* 51{"  "} */}
                        {item.telefone.substring(0, 2)}
                        {""}
                        <Text
                          fontWeight="bold"
                          style={{
                            fontSize: RF(1),
                          }}>
                          {item.telefone.substring(2)}
                        </Text>
                      </Text>
                    </View>
                  </View>
                </View>
              ))}
            </View>
          </View>
        </ImageBackground>
      </View>
    </ScrollView>
  );
}
