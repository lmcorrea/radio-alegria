
export const Colors = {
  yellow: "#CCBE42",
  red: "#E7451E",
  boldGreen: "#044935",
  mediumGreen: "#17735c",
  green:"#006C64",
  lightGreen: "#06A388",
  gray: "#EDEDED",
}
  