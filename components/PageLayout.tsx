import { Colors } from "@/constants/Colors";
import responsiveSizes from "@/utils/responsiveSizes";
import { LinearGradient, LinearGradientProps } from "expo-linear-gradient";
import React from "react";
import {
  Image,
  Platform,
  View,
  ImageBackground,
  Dimensions,
  PixelRatio,
  ViewStyle,
} from "react-native";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { moderateScale } from "react-native-size-matters";

interface PageLayoutProps {
  children: React.ReactNode;
  headerTitle: any;
  whitePage?: boolean;
  linearGradient?: boolean;
  linearGradientProps?: LinearGradientProps;
  whitePageStyle?: ViewStyle;
}

const PageLayout = ({
  children,
  headerTitle,
  whitePage = true,
  whitePageStyle,
  linearGradient = false,
  linearGradientProps,
}: PageLayoutProps) => {
  const { isMobile, RW, RH, RF } = responsiveSizes();

  if (linearGradient) {
    return (
      <LinearGradient
        colors={[Colors.lightGreen, Colors.green]}
        locations={[0.3, 0.9]}
        style={{
          width: "100%",
          alignItems: "center",
          height: "auto",
        }}
        {...linearGradientProps}>
        <ImageBackground
          source={
            isMobile
              ? require("@/assets/images/receitas/Ativo 7.png")
              : require("@/assets/images/receitas/Ativo 3.png")
          }
          resizeMode="stretch"
          style={{
            width: "100%",
            height: isMobile ? RW(32) : RW(18),
            alignItems: "center",
            position: "relative",
            zIndex: 2,
          }}>
          {headerTitle}
        </ImageBackground>
        {whitePage ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              backgroundColor: "white",
              minWidth: RW(75),
              marginTop: isMobile
                ? responsiveScreenHeight(-5)
                : responsiveScreenHeight(-10),
              paddingTop: isMobile
                ? responsiveScreenHeight(6)
                : responsiveScreenHeight(12),
              paddingBottom: isMobile
                ? responsiveScreenHeight(4)
                : responsiveScreenHeight(5),
              paddingHorizontal: RW(3),
              marginHorizontal: RW(20),
              ...whitePageStyle,
            }}>
            {children}
          </View>
        ) : (
          <>{children}</>
        )}
      </LinearGradient>
    );
  }

  return (
    <ImageBackground
      source={
        isMobile
          ? require("@/assets/images/receitas/Ativo 8.png")
          : require("@/assets/images/receitas/Ativo 4.png")
      }
      style={{
        width: "100%",
        alignItems: "center",
        height: "auto",
      }}>
      <ImageBackground
        source={
          isMobile
            ? require("@/assets/images/receitas/Ativo 7.png")
            : require("@/assets/images/receitas/Ativo 3.png")
        }
        resizeMode="stretch"
        style={{
          width: "100%",
          height: moderateScale(120),
          justifyContent: "center",
          alignItems: "center",
          paddingBottom: isMobile
            ? responsiveScreenHeight(2.5)
            : responsiveScreenHeight(5),
          position: "relative",
          zIndex: 2,
        }}>
        {headerTitle}
      </ImageBackground>
      {whitePage ? (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            backgroundColor: "white",
            minWidth: RW(75),
            marginTop: isMobile
              ? responsiveScreenHeight(-5)
              : responsiveScreenHeight(-10),
            paddingTop: isMobile
              ? responsiveScreenHeight(6)
              : responsiveScreenHeight(12),
            paddingBottom: isMobile
              ? responsiveScreenHeight(4)
              : responsiveScreenHeight(5),
            paddingHorizontal: RW(5),
            marginHorizontal: RW(10),
            ...whitePageStyle,
          }}>
          {children}
        </View>
      ) : (
        <>{children}</>
      )}
    </ImageBackground>
  );
};
export default PageLayout;
