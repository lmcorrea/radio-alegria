import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  Platform,
  Dimensions,
  TextInputProps,
  ViewProps,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import {
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";

import { Colors } from "@/constants/Colors";
import { scale } from "react-native-size-matters";
import { StyleProps } from "react-native-reanimated";

interface InputProps {
  password?: boolean;
  placeholder?: string;
  icon?: React.ReactNode;
  setText: (text: string) => void;
  type?: any;
  value?: string;
  error?: string;
  disabled?: boolean;
  StyleInputProps?: StyleProps;
  WrapperProps?: StyleProps;
  InputProps?: TextInputProps;
}

export default function Input({
  password,
  placeholder,
  icon,
  setText,
  type = "default",
  value,
  error,
  disabled,
  StyleInputProps,
  WrapperProps,
  InputProps,
}: InputProps) {
  const [backgroundFocus, setBackgroundFocus] = useState(false);
  const isMobile = Dimensions.get("window").width < 768;
  const [showPassword, setShowPassword] = useState<boolean>(false);

  return (
    <>
      <View
        focusable={backgroundFocus}
        style={{
          flexDirection: "row",
          width: "100%",
          borderRadius: isMobile ? 20 : 50,
          borderWidth: 1,
          height: scale(25),
          backgroundColor: "white",
          borderColor: error ? "red" : backgroundFocus ? "#333" : "#DDDDDD",
          alignItems: "center",
          ...WrapperProps,
        }}>
        {icon && <Text style={{ marginLeft: scale(15) }}>{icon}</Text>}
        <TextInput
          editable={!disabled}
          keyboardType={type}
          placeholder={placeholder}
          style={{
            fontFamily: "regular",
            marginLeft: isMobile
              ? responsiveScreenWidth(2)
              : responsiveScreenWidth(1),
            width: isMobile ? "85%" : "90%",
            ...(Platform.OS === "web" && { outlineWidth: 0 }),
            ...StyleInputProps,
          }}
          selectTextOnFocus={false}
          onFocus={() => setBackgroundFocus(true)}
          onBlur={() => setBackgroundFocus(false)}
          autoFocus={false}
          focusable={false}
          onChangeText={(text) => setText(text)}
          secureTextEntry={(password && !showPassword) || false}
          value={value}
          {...InputProps}
          
        />
        {password && (
          <Text
            style={{ alignItems: "flex-end" }}
            onPress={() => setShowPassword(!showPassword)}>
            <Icon
              name={showPassword ? "eye" : "eye-slash"}
              size={
                isMobile
                  ? responsiveScreenFontSize(2.2)
                  : responsiveScreenFontSize(1.2)
              }
              color={"black"}
            />
          </Text>
        )}
      </View>
      {error && <Text style={{ color: "red" }}>{error}</Text>}
    </>
  );
}
