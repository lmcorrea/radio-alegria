import {
  Dimensions,
  Image,
  Platform,
  Pressable,
  Touchable,
  View,
  TouchableHighlight,
} from "react-native";
import Text from "../Text";
import {
  responsiveFontSize,
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { Colors } from "@/constants/Colors";
import { router } from "expo-router";
import React from "react";
import { scale, verticalScale } from "react-native-size-matters";
import { colors, vertical } from "react-native-swiper-flatlist/src/themes";
import { LinearGradient } from "expo-linear-gradient";

const PolaroidImages = () => {
  const space = 200;
  const isMobile = Dimensions.get("window").width < 768;

  const imgs = [
    {
      top: 40,
      rotate: 10,
    },
    {
      top: 0,
      rotate: -15,
    },
    {
      top: 40,
      rotate: 15,
    },
    {
      top: 0,
      rotate: 3,
    },
  ];

  return (
    <Pressable
      onPress={() => router.push("/galeria")}
      style={{
        marginLeft: "7%",
        height: responsiveScreenFontSize(20),
        flexDirection: "row",
        gap: 0,
        alignItems: "center",
      }}>
      <LinearGradient
        colors={["transparent", "rgb(0, 108, 100)"]}
        style={{
          alignItems: "center",
          padding: 15,
          paddingVertical: 40,
          paddingLeft: 50,
          width: "30%",
        }}
        start={{ x: 0, y: 0.9 }}
        end={{ x: 1, y: 0 }}
        locations={[0.1, 0.4]}>
        <View>
          <Text style={{ fontSize: 22, color: "yellow" }}>#TÔNAALEGRIA</Text>
          <Text>CLIQUE E CONFIRA</Text>
        </View>
      </LinearGradient>

      <Image
        source={require("@/assets/images/fotos-home-desk.png")}
        style={{
          width: "63%",
          height: "100%",
          left: -70,
        }}
        resizeMode="contain"
      />
    </Pressable>
  );
};

export const MobilePolaroidImages = () => {
  const imgs = [
    {
      top: 80,
      rotate: 20,
      zIndex: 3,
      left: 100,
    },
    {
      top: 150,
      rotate: -5,
      zIndex: 4,
      left: 120,
    },
    {
      top: 140,
      rotate: 15,
      zIndex: 0,
      left: 230,
    },
    {
      top: 60,
      rotate: -15,
      zIndex: 1,
      left: 220,
    },
  ];

  return (
    <Pressable
      onPress={() => router.push("/galeria")}
      style={{
        height: 200,
        marginTop: 30,
      }}>
      <View
        style={{
          top: 50,
          display: "flex",
          position: "absolute",
          width: responsiveScreenWidth(80),
          backgroundColor: "rgb(0, 108, 100)",
          paddingVertical: responsiveScreenHeight(3),
          paddingLeft: responsiveScreenWidth(3),
        }}>
        <Text style={{ fontSize: responsiveFontSize(2), color: "yellow" }}>
          #TÔNAALEGRIA
        </Text>
        <Text style={{ fontSize: responsiveFontSize(1) }}>
          CLIQUE E CONFIRA
        </Text>
      </View>
      <Image
        source={require("@/assets/images/fotos-home-mobile.png")}
        style={{
          alignSelf: "flex-end",
          width: "55%",
          height: 200,
        }}
      />
    </Pressable>
  );
};

export default PolaroidImages;
