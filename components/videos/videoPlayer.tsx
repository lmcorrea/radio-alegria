import React from "react";
import { View, Text, Platform } from "react-native";
import YoutubePlayer from "react-native-youtube-iframe";

export default function VideoPlayer({ video }) {
  return (
    <View>
      <YoutubePlayer
        height={200}
        play={true}
        videoId={video.url}
        webViewProps={{
          allowsFullscreenVideo: true,
          allowsInlineMediaPlayback: true,
        }}
      />
      <Text>{video.title}</Text>
    </View>
  );
}
