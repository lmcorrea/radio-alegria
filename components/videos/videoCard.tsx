import { Video } from "@/app/videos";
import { Colors } from "@/constants/Colors";
import { router } from "expo-router";
import React from "react";
import {
  View,
  Text,
  ImageBackground,
  Platform,
  Dimensions,
  Image,
  Pressable,
} from "react-native";
import {
  responsiveFontSize,
  responsiveScreenFontSize,
  responsiveScreenHeight,
} from "react-native-responsive-dimensions";
import { scale } from "react-native-size-matters";

export default function VideoCard({ video }: { video: Video }) {
  const isMobile = Dimensions.get("window").width < 768;
  return (
    <Pressable
      style={{
        width: scale(160),
        backgroundColor: Colors.yellow,
        padding: scale(10),
        justifyContent: "space-between",
        marginTop: scale(25),
      }}
      onPress={() => {
        router.push(`videos/${video.id}`);
      }}
    >
      <View>
        <Text
          numberOfLines={1}
          style={{
            color: "black",
            fontSize: responsiveScreenFontSize(1.5),
            fontWeight: "bold",
          }}
        >
          {video.title}
        </Text>
        <Text
          style={{
            color: "black",
            fontSize: responsiveScreenFontSize(1.1),
          }}
        >
          {video.date_publish_init.replaceAll("/", ".")}
        </Text>
      </View>
      <Image
        source={{ uri: video.links.cover }}
        style={{
          marginTop: scale(10),
          width: scale(140),
          height: scale(180),
        }}
      />
    </Pressable>
  );
}
