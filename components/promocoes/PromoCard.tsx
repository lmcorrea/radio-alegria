import { Image, View, StyleSheet, Pressable, Dimensions } from "react-native";
import {
  responsiveFontSize,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { router } from "expo-router";
import Text from "../Text";
import { scale } from "react-native-size-matters";
import { loginStore } from "@/store/Login";
import responsiveSizes from "@/utils/responsiveSizes";

interface CardType {
  id: number;
  title: string;
  image: string;
}

const isMobile = Dimensions.get("window").width < 768;

const PromoCard = ({ item, url }: { item: CardType; url: string }) => {
  const { isMobile, RW, RF, RH } = responsiveSizes();

  const handleNavigate = () => {
    router.push(url + "/" + item.id);
  };

  return (
    <Pressable
      onPress={handleNavigate}
      style={{
        alignItems: "center",
        margin: 10,
        marginBottom: 25,
        width: isMobile ? "100%" : RW(30),
      }}>
      <Image
        source={{
          uri: item.image,
        }}
        style={{
          width: "100%",
          height: RF(20),
          marginBottom: 15,
        }}
      />

      <Text
        style={{
          color: "#000",
          fontSize: RF(1.2),
          marginBottom: isMobile ? 5 : 10,
          marginTop: isMobile ? 0 : 20,
          flexShrink: 1,
          textAlign: "left",
          width: "100%",
          ...(!isMobile && {
            maxHeight: 65,
            height: "100%",
          }),
        }}
        numberOfLines={2}>
        {item.title}
      </Text>

      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          width: "100%",
          alignItems: "center",
        }}>
        <Pressable onPress={handleNavigate}>
          <Image
            source={require("@/assets/images/promocoes/quero_participar_desk.png")}
            style={{
              width: 160,
              height: 45,
              resizeMode: "contain",
            }}
          />
        </Pressable>
      </View>
    </Pressable>
  );
};

export default PromoCard;
