import { PromotionDetails } from "@/app/promocoes/[id]";
import { Dimensions, Pressable, View } from "react-native";
import Input from "../Input";
import Text from "../Text";
import { useState } from "react";
import { authenticatedUrl } from "@/services/api";
import { Colors } from "@/constants/Colors";
import { moderateScale, verticalScale } from "react-native-size-matters";
import { BackButton } from "../BackButton";
import { router } from "expo-router";
import { useToast } from "react-native-toast-notifications";

const isMobile = Dimensions.get("window").width < 768;

const PromocoesInputHandler = ({
  promotion,
  setClickParticipate,
}: {
  promotion: PromotionDetails;
  setClickParticipate: (value: boolean) => void;
}) => {
  if (promotion.type === "question_answer") {
    return (
      <QuestionAnswerPromotion
        promotion={promotion}
        setClickParticipate={setClickParticipate}
      />
    );
  }

  if (promotion.type === "simple") {
    return (
      <SimplePromotion
        promotion={promotion}
        setClickParticipate={setClickParticipate}
      />
    );
  }

  return <></>;
};

const QuestionAnswerPromotion = ({
  promotion,
  setClickParticipate,
}: {
  promotion: PromotionDetails;
  setClickParticipate: (value: boolean) => void;
}) => {
  const [answer, setAnswer] = useState("");
  const [cpf, setCpf] = useState("");
  const toast = useToast();

  const handleSendAnswer = async () => {
    try {
      const res = await authenticatedUrl.post(
        `controla/promotion/participate/${promotion.id}`,
        {
          answer,
          cpf,
        }
      );
      //router.push("/promocoes");
      toast.show("Resposta enviada com sucesso", {
        type: "success",
        placement: "bottom",
      });
      router.back();
    } catch (error: any) {
      toast.show("Erro ao enviar resposta", {
        type: "danger",
        placement: "bottom",
      });
    }
  };

  return (
    <View style={{ gap: 8, width: "90%", alignItems: "center" }}>
      <View
        style={{
          paddingHorizontal: 5,
          paddingVertical: 10,
          backgroundColor: Colors.yellow,
          borderRadius: 20,
          top: 15,
          zIndex: 1,
        }}>
        <Text
          style={{
            color: "white",
            fontSize: moderateScale(13),
            textAlign: "center",
          }}>
          {promotion.question}
        </Text>
      </View>
      <Input
        placeholder="Resposta"
        value={answer}
        setText={setAnswer}
        WrapperProps={{
          backgroundColor: "white",
          height: verticalScale(80),
          width: "100%",
          borderRadius: 20,
        }}
        StyleInputProps={{
          height: verticalScale(70),
          width: "100%",
        }}
        InputProps={{
          multiline: true,
          numberOfLines: 10,
          textAlignVertical: "top",
          maxLength: 900,
        }}
      />
      {promotion.enable_cpf && (
        <Input
          placeholder="CPF"
          value={cpf}
          setText={setCpf}
          WrapperProps={{
            backgroundColor: "white",
            height: verticalScale(60),
            width: "100%",
            borderRadius: 20,
          }}
          StyleInputProps={{
            height: verticalScale(50),
            width: "100%",
          }}
          InputProps={{
            keyboardType: "numeric",
          }}
        />
      )}

      <Pressable
        onPress={handleSendAnswer}
        style={{
          backgroundColor: Colors.yellow,
          paddingVertical: moderateScale(8),
          paddingHorizontal: moderateScale(15),
          borderRadius: 20,
          alignSelf: "flex-end",
        }}>
        <Text>Enviar</Text>
      </Pressable>
      <BackButton onPress={() => setClickParticipate(false)} />
    </View>
  );
};

const SimplePromotion = ({
  promotion,
  setClickParticipate,
}: {
  promotion: PromotionDetails;
  setClickParticipate: (value: boolean) => void;
}) => {
  const toast = useToast();
  const [cpf, setCpf] = useState("");

  const handleParticipate = async () => {
    try {
      await authenticatedUrl.post(
        `controla/promotion/participate/${promotion.id}`, {
          cpf,
        }
      );
      toast.show("Participação realizada com sucesso", {
        type: "success",
        placement: "bottom",
      });
      router.push("/promocoes");
    } catch (error: any) {
      console.log(error.message);
      toast.show("Erro ao participar da promoção", {
        type: "danger",
        placement: "bottom",
      });
    }
  };

  return (
    <View style={{ width: "100%", alignItems: "center", gap: 20 }}>
      <Pressable
        onPress={handleParticipate}
        style={{
          backgroundColor: Colors.yellow,
          paddingVertical: verticalScale(8),
          paddingHorizontal: verticalScale(18),
          borderRadius: 20,
          width: isMobile ? "80%" : "100%",
        }}>
        <Text style={{ textAlign: "center", fontSize: moderateScale(14) }}>
          Enviar
        </Text>
      </Pressable>

      {promotion.enable_cpf && (
        <Input
          placeholder="CPF"
          value={cpf}
          setText={setCpf}
          WrapperProps={{
            backgroundColor: "white",
            width: "100%",
            borderRadius: 20,
          }}
          StyleInputProps={{
            width: "100%",
          }}
          InputProps={{
            keyboardType: "numeric",
          }}
        />
      )}

      <BackButton
        textColor="black"
        onPress={() => setClickParticipate(false)}
      />
    </View>
  );
};

export default PromocoesInputHandler;
