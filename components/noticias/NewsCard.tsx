import { Colors } from "@/constants/Colors";
import {
  Image,
  View,
  StyleSheet,
  Pressable,
  Dimensions,
  StyleSheetProperties,
} from "react-native";
import { StyleProps } from "react-native-reanimated";
import {
  responsiveScreenFontSize,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { router } from "expo-router";
import Text from "../Text";
import { scale, verticalScale } from "react-native-size-matters";
import responsiveSizes from "@/utils/responsiveSizes";

interface CardType {
  id: number;
  title: string;
  description: string;
  image: string;
}

const isMobile = Dimensions.get("window").width < 768;

const NewsCard = ({
  item,
  cardProps,
}: {
  item: CardType;
  cardProps?: StyleProps;
}) => {
  const { isMobile, RW, RH, RF } = responsiveSizes();

  const styles = StyleSheet.create({
    pressable: {
      elevation: 20,
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.25,
      shadowRadius: 8,
    },
    card: {
      flexDirection: "row",
      alignItems: "center",
      width: isMobile ? RW(40) : RW(25),
      backgroundColor: Colors.gray,
      height: RF(9),
      padding: 5,
    },
    title: {
      color: Colors.green,
      fontSize: RF(1),
    },
    subtitle: {
      color: Colors.yellow,
      fontSize: RF(0.9),
    },
    image: {
      width: RF(8),
      height: "100%",
      resizeMode: "stretch",
    },
    textContainer: {
      paddingLeft: RF(1),
      height: "100%",
      flex: 1,
      textAlign: "left",
      justifyContent: "space-between",
      paddingVertical: RF(0.5),
    },
  });

  return (
    <Pressable
      onPress={() => router.replace(`noticias/${item.id}`)}
      style={styles.pressable}>
      <View style={{ ...styles.card, ...cardProps }}>
        <Image
          source={{
            uri: item.image,
          }}
          style={styles.image}
        />
        <View style={styles.textContainer}>
          <Text style={styles.title} numberOfLines={5} lineBreakMode="tail">
            {item.title}
          </Text>
          <Text style={styles.subtitle}>{`LEIA MAIS >`}</Text>
        </View>
      </View>
    </Pressable>
  );
};

export default NewsCard;
