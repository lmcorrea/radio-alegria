import React from "react";
import { Platform, StyleSheet, View, StyleProp } from "react-native";

const HtmlContent = ({ html, styles } : { html: string, styles: React.CSSProperties  }) => {
  if (Platform.OS === "web") {
    return (
      <div
        style={styles}
              

        dangerouslySetInnerHTML={{ __html: html }}
      />
    );
  }

  // Fallback for other platforms
  return <View style={styles.fallbackContainer}>{/* Fallback UI */}</View>;
};

const styles = StyleSheet.create({
  webContainer: {
    // Styles for your web container
  },
  fallbackContainer: {
    // Styles for your fallback container
  },
});

export default HtmlContent;
