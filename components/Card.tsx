import { Colors } from "@/constants/Colors";
import {
  Image,
  View,
  StyleSheet,
  Pressable,
  Platform,
  Dimensions,
} from "react-native";
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
  useDimensionsChange,
} from "react-native-responsive-dimensions";
import { router } from "expo-router";
import Text from "./Text";
import { moderateScale, scale } from "react-native-size-matters";
import { useCallback, useState } from "react";
import responsiveSizes from "@/utils/responsiveSizes";

export type CardType = {
  id: number;
  headline: string;
  description: string;
  highlight: number;
  author: string;
  category_multimedia: {
    name: string;
  };
  date_publish_init: string;
  date_publish_end: string;
  links: {
    cover: string;
    view?: string;
  };
  videos: any[];
  audios: any[];
};

const Card = ({ item, url }: { item: CardType; url: string }) => {
  const { isMobile, RW, RH, RF } = responsiveSizes();

  return (
    <Pressable
      onPress={() => router.navigate(`${url}/${item.id}`)}
      style={{
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
        gap: isMobile ? RF(1) : RF(2),
        paddingLeft: RW(1),
      }}>
      <Image
        source={{ uri: item.links.cover }}
        style={{
          width: isMobile ? RF(15) : RF(25),
          height: isMobile ? RF(12) : RF(15),
          resizeMode: "stretch",
        }}
      />
      <View>
        <Text
          style={{
            color: Colors.green,
            fontSize: RF(1.8),
            fontWeight: "bold",
            marginBottom: 10,
            flexShrink: 1,
            maxWidth: isMobile ? RW(45) : RW(30),
            textAlign: "left",
          }}>
          {item.headline}
        </Text>
        <Text
          style={{ fontSize: RF(1), color: Colors.yellow }}
          onPress={() =>
            router.navigate(`${url}/${item.id}`)
          }>{`Leia mais >`}</Text>
      </View>
    </Pressable>
  );
};

export default Card;
