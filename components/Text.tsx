import { Text as T, type TextProps, StyleSheet } from "react-native";

export type ThemedTextProps = TextProps & {
  fontWeight?: "regular" | "bold" | "semiBold" | "medium" | "thin" | "light";
};

export default function Text({
  fontWeight = "regular",
  children,
  ...rest
}: ThemedTextProps) {
  return (
    <T
      {...rest}
      style={{
        fontFamily: `${fontWeight}`,
        color: "#FFF",
        ...StyleSheet.flatten(rest.style),	
      }}>
      {children}
    </T>
  );
}
