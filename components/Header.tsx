import {
  Image,
  Platform,
  View,
  Pressable,
  ImageBackground,
  PixelRatio,
  TouchableOpacity,
  Dimensions,
  Linking,
} from "react-native";
import Text from "@/components/Text";
import { Colors } from "@/constants/Colors";
import { FontAwesome, FontAwesome5 } from "@expo/vector-icons";
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";

import promocoes from "@/assets/images/promocoes.png";
import programacao from "@/assets/images/programacao.png";
import receitas from "@/assets/images/receitas.png";
import videos from "@/assets/images/videos.png";

import { router } from "expo-router";
import TrackPlayer, {Event} from "react-native-track-player";
import { playStore } from "@/store/PlayStore";
import { moderateScale, scale } from "react-native-size-matters";
import React from "react";

const Header = () => {
  const isMobile = Dimensions.get("window").width < 768;
  const { radioPlaying, setRadioPlaying, audioRef, radioInfo } = playStore();

  const RadioStations = () => {

    const playRadio = async (radio: any) => {
      if (radio.link_streaming === radioPlaying?.link_streaming) {
        console.log("stop");
        await TrackPlayer.reset();
        return setRadioPlaying("");
      } else {
        console.log("play", radio.link_streaming);
        await TrackPlayer.reset();
        await TrackPlayer.add({
          url: radio.link_streaming,
          title: radio.fixed_text_rds,
          artist: "",
          artwork: radio.cover.uri,
        });

        await TrackPlayer.play();
        return setRadioPlaying(radio);
      }
    };

    const playRadioWeb = (radio: any) => {
      if (radio.link_streaming === radioPlaying?.link_streaming) {
        console.log("pausar");
        audioRef.current?.pause();
        setRadioPlaying("");
      } else {
        if (!audioRef.current) return;
        audioRef.current?.pause();
        audioRef.current.src = radio.link_streaming;
        audioRef.current?.play();
        setRadioPlaying(radio);
      }
    };

    const styles = Platform.select({
      web: {
        text: responsiveScreenFontSize(0.8),
        porto_alegre: isMobile
          ? { width: moderateScale(70), height: moderateScale(25) }
          : { width: moderateScale(100), height: moderateScale(35) },
        pelotas: isMobile
          ? { width: moderateScale(63), height: moderateScale(25) }
          : { width: moderateScale(100), height: moderateScale(35) },
        gap: 10,
      },
      default: {
        text: responsiveScreenFontSize(1.4),
        porto_alegre: { width: moderateScale(95), height: moderateScale(30) },
        pelotas: { width: moderateScale(95), height: moderateScale(30) },
        gap: 20,
      },
    });

    TrackPlayer.addEventListener(Event.RemoteStop, () => {
      TrackPlayer.stop();
      setRadioPlaying({...radioPlaying, stopped: true});
    });
    TrackPlayer.addEventListener(Event.RemotePlay, () => {
      TrackPlayer.play();
      setRadioPlaying({...radioPlaying, stopped: false});
    });

    TrackPlayer.addEventListener(Event.RemotePause, () => {
      TrackPlayer.pause();
      setRadioPlaying({...radioPlaying, stopped: true});
    });

    return (
      <View
        style={{
          borderWidth: 1,
          flexDirection: "row",
          borderRadius: 10,
          borderColor: "#FFF",
          gap: styles.gap,
          width: "auto",
          padding: 10,
          marginTop: isMobile ? 20 : 0,
          marginBottom: 20,
          justifyContent: "center",
          alignItems: "center",
          //marginHorizontal: isMobile ? 0 : 30,
        }}>
        <View style={{ alignItems: "center" }}>
          <Text style={{ fontSize: styles.text }}>
            <Text fontWeight="bold">92.9 FM</Text> - PORTO ALEGRE
          </Text>

          <Pressable
            onPress={() => {
              Platform.OS === "web"
                ? playRadioWeb(radioInfo.find((radio) => radio.id === 374))
                : playRadio(radioInfo.find((radio) => radio.id === 374));
            }}>
            <Image
              source={
                radioPlaying.id === 374 && !radioPlaying?.stopped
                  ? require("@/assets/images/botao-pausa-porto.png")
                  : require("@/assets/images/aoVivoVerde.png")
              }
              resizeMode="contain"
              style={styles.porto_alegre}
            />
          </Pressable>
        </View>
        <View style={{ alignItems: "center" }}>
          <Text
            style={{
              fontSize: styles.text,
            }}>
            <Text fontWeight="bold">89.5 FM</Text> - PELOTAS
          </Text>
          <Pressable
            onPress={() =>
              Platform.OS === "web"
                ? playRadioWeb(radioInfo.find((radio) => radio.id === 221))
                : playRadio(radioInfo.find((radio) => radio.id === 221))
            }>
            <Image
              source={
                radioPlaying.id === 221 && !radioPlaying?.stopped
                  ? require("@/assets/images/botao-pausa-pelotas.png")
                  : require("@/assets/images/aoVivoAmarelo.png")
              }
              resizeMode="contain"
              style={styles.pelotas}
            />
          </Pressable>
        </View>
      </View>
    );
  };

  const AppDownload = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          zIndex: 0,
          top: 10,
          padding: 0,
          margin: 0,
          width: "45%",
        }}>
        <Image
          source={require("@/assets/images/iphoneMao.png")}
          resizeMode="contain"
          style={{
            width: moderateScale(100),
            height: moderateScale(120),
            zIndex: 5,
          }}
        />
        <View
          style={{
            gap: 5,
            left: -scale(20),
            width: isMobile
              ? responsiveScreenWidth(28)
              : responsiveScreenWidth(16),
          }}>
          <Text
            style={{
              marginTop: 20,
              fontSize: responsiveScreenFontSize(1),
            }}>
            ENTRE NO MUNDO DO AGRO, BAIXE O APP
          </Text>
          <View style={{ gap: 10, left: -20 }}>
            <Pressable
              onPress={() => {
                Linking.openURL(
                  "https://play.google.com/store/apps/details?id=br.com.radioalegria.superaudio"
                );
              }}
              style={{
                backgroundColor: "#fff",
                padding: isMobile ? 2 : 8,
                paddingLeft: isMobile ? 25 : 50,
                width: "100%",
                borderRadius: 5,
                flexDirection: "row",
                alignItems: "center",
                gap: 5,
              }}>
              <Image
                source={require("@/assets/images/playstore.png")}
                style={{ width: scale(12), height: 20 }}
                resizeMode="contain"
              />
              <Text
                fontWeight="bold"
                style={{
                  color: Colors.boldGreen,
                  textAlign: "center",
                  fontSize: responsiveScreenFontSize(0.9),
                }}>
                GOOGLE PLAY
              </Text>
            </Pressable>
            <Pressable
              onPress={() => {
                Linking.openURL(
                  "https://apps.apple.com/it/app/rádio-alegria-coração-do-agro/id6740045508"
                );
              }}
              style={{
                backgroundColor: "#fff",
                padding: isMobile ? 2 : 8,
                paddingLeft: isMobile ? 25 : 50,
                width: "100%",
                borderRadius: 5,
                flexDirection: "row",
                alignItems: "center",
                gap: 5,
              }}>
              <Image
                source={require("@/assets/images/apple.png")}
                style={{ width: scale(12), height: 20 }}
                resizeMode="contain"
              />
              <Text
                fontWeight="bold"
                style={{
                  color: Colors.boldGreen,
                  textAlign: "center",
                  fontSize: responsiveScreenFontSize(0.9),
                }}>
                APPLE STORE
              </Text>
            </Pressable>
          </View>
        </View>
      </View>
    );
  };

  const ButtonsHomeHeader = ({ nameImage }: { nameImage: string }) => {
    const images = {
      promocoes,
      programacao,
      receitas,
      videos,
    } as any;

    return (
      <Pressable onPress={() => router.navigate(`/${nameImage}`)}>
        {nameImage === "promocoes" ? (
          <Image
            source={images[nameImage]}
            style={{
              width: isMobile
                ? responsiveScreenWidth(47)
                : responsiveScreenWidth(16),
              height: isMobile
                ? responsiveScreenWidth(8)
                : responsiveScreenHeight(6),
              left: isMobile ? -3 : 0,
            }}
          />
        ) : (
          <Image
            source={images[nameImage]}
            style={{
              width: isMobile
                ? responsiveScreenWidth(45)
                : responsiveScreenWidth(16),
              height: isMobile
                ? responsiveScreenWidth(8)
                : responsiveScreenHeight(6),
            }}
          />
        )}
      </Pressable>
    );
  };

  const Logo = () => {
    return (
      <Pressable onPress={() => router.navigate("/")}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            gap: 50,
            marginTop: isMobile ? 20 : 0,
          }}>
          <Image
            style={{
              width: isMobile
                ? responsiveScreenWidth(46.9)
                : responsiveScreenWidth(12.9),
              height: responsiveScreenHeight(10.7),
            }}
            resizeMode="contain"
            source={require("@/assets/images/logo.png")}
          />
          {isMobile && (
            <View
              style={{
                backgroundColor: "#FFF",
                padding: 10,
                borderRadius: 10,
              }}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  gap: 10,
                }}>
                <Text
                  fontWeight="medium"
                  style={{ color: Colors.yellow, fontSize: 20 }}>
                  SIGA:
                </Text>
                <Pressable
                  onPress={() =>
                    Linking.openURL("https://www.facebook.com/radioalegria/")
                  }>
                  <FontAwesome
                    name="facebook-square"
                    size={34}
                    color={Colors.yellow}
                  />
                </Pressable>
                <Pressable
                  onPress={() =>
                    Linking.openURL("https://www.instagram.com/radio_alegria/")
                  }>
                  <FontAwesome5
                    name="instagram-square"
                    size={34}
                    color={Colors.yellow}
                  />
                </Pressable>
              </View>
            </View>
          )}
        </View>
      </Pressable>
    );
  };

  return (
    <ImageBackground
      source={require("@/assets/images/background.png")}
      style={{
        flex: 1,
        width: "101%",
        marginLeft: -1,
        height: "90%",
      }}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          flexWrap: "wrap",
          marginTop: isMobile ? 30 : 0,
        }}>
        <Logo />
        <View
          style={{
            width: isMobile ? "100%" : "60%",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            gap: isMobile ? 0 : 10,
          }}>
          <RadioStations />
          {Platform.OS == "web" && <AppDownload />}
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            gap: 10,
            flexWrap: "wrap",
            width: "100%",
            backgroundColor: Colors.lightGreen,
            paddingVertical: 10,
            borderTopWidth: 7,
            borderBottomWidth: 7,
            borderColor: "#FFF",
          }}>
          <ButtonsHomeHeader nameImage="promocoes" />
          <ButtonsHomeHeader nameImage="programacao" />
          <ButtonsHomeHeader nameImage="receitas" />
          <ButtonsHomeHeader nameImage="videos" />
          {!isMobile && (
            <View
              style={{
                backgroundColor: "#FFF",
                borderRadius: 10,
              }}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  flex: 1,
                  paddingHorizontal: 8,
                  gap: 10,
                }}>
                <Text
                  fontWeight="medium"
                  style={{ color: Colors.yellow, fontSize: 18 }}>
                  Siga:
                </Text>
                <Pressable
                  onPress={() => {
                    Linking.openURL(radioInfo[0].link_facebook);
                  }}>
                  <FontAwesome
                    name="facebook-square"
                    size={24}
                    color={Colors.yellow}
                  />
                </Pressable>
                <Pressable
                  onPress={() => {
                    Linking.openURL(radioInfo[0].link_instagram);
                  }}>
                  <FontAwesome5
                    name="instagram-square"
                    size={24}
                    color={Colors.yellow}
                  />
                </Pressable>
              </View>
            </View>
          )}
        </View>
      </View>
    </ImageBackground>
  );
};

export default React.memo(Header);
