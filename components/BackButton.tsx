import { Dimensions, Image, TouchableOpacity } from "react-native";
import Text from "./Text";
import { Colors } from "@/constants/Colors";
import { moderateScale, scale, verticalScale } from "react-native-size-matters";
import { router } from "expo-router";

type BackButtonProps = {
  onPress?: () => void;
  textColor?: string;
};

export const BackButton = ({ onPress = () => router.back(), textColor = Colors.boldGreen }: BackButtonProps) => {
  const isMobile = Dimensions.get("window").width < 768;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        display: "flex",
        flexDirection: "row",
        width: 80,
        alignItems: "center",
        justifyContent: "flex-start",
        padding: 5,
        gap: 3,
        marginTop: 30,
      }}
    >
      <Image
        source={
          isMobile
            ? require("@/assets/images/back_arrow.png")
            : require("@/assets/images/back_arrow.png")
        }
        style={{
          width: scale(20),
          height: verticalScale(12),
          marginBottom: -3,
        }}
        resizeMode="contain"
      />
      <Text style={{ color: textColor, fontSize: moderateScale(13) }}>
        voltar
      </Text>
    </TouchableOpacity>
  );
};
