import React from "react";
import { View, Image } from "react-native";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import responsiveSizes from "../utils/responsiveSizes";
import { Colors } from "../constants/Colors";
import { Ionicons } from "@expo/vector-icons";
import Text from "./Text";

type ProgramacaoItemProps = {
  item: {
    time_start: string;
    time_end: string;
    cover: string;
    description: string;
  };
};

export default function ProgramacaoItem({ item }: ProgramacaoItemProps) {
  const { RF, RW, RH } = responsiveSizes();

  return (
    <Collapse
      TouchableOpacityProps={{
        activeOpacity: 1,
      }}
      style={{
        zIndex: 5,
        width: "100%",
      }}>
      <CollapseHeader
        style={{
          zIndex: 4,
          width: "100%",
          height: RF(7.5),
        }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            flexWrap: "wrap",
            alignSelf: "center",
            zIndex: 1,
            height: "100%",
            width: "100%",
            flex: 1,
          }}>
          <View
            style={{
              backgroundColor: Colors.boldGreen,
              borderWidth: 3,
              borderColor: "#FFF",
              borderBottomLeftRadius: 10,
              borderTopLeftRadius: 10,
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
              width: "20%",
              height: "100%",
            }}>
            <Text
              style={{
                fontSize: RF(1.6),
              }}>
              {item.time_start} | {item.time_end}{" "}
            </Text>
          </View>
          <View
            style={{
              backgroundColor: "#FFF",
              borderColor: "black",
              justifyContent: "center",
              alignItems: "center",
              flex: 1,
              height: "100%",
            }}>
            <Image
              resizeMode="contain"
              source={{
                uri: item.cover,
              }}
              style={{
                width: "100%",
                height: "100%",
              }}
            />
          </View>
          <View
            style={{
              backgroundColor: "#CCBD3E",
              borderWidth: 3,
              borderColor: "#FFF",
              borderBottomRightRadius: 10,
              borderTopRightRadius: 10,
              flex: 1,
              width: "20%",
              height: "100%",
              flexDirection: "row",
              gap: 5,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={{
                fontSize: RF(1.6),
                color: Colors.boldGreen,
              }}>
              Saiba Mais
            </Text>
            <Ionicons
              name="chevron-down-circle-outline"
              size={RF(2)}
              color={Colors.boldGreen}
            />
          </View>
        </View>
      </CollapseHeader>
      <CollapseBody
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          flexWrap: "wrap",
          alignSelf: "center",
          width: "100%",
          top: -20,
          zIndex: -1,
        }}>
        <View
          style={{
            backgroundColor: "#DADADA",
            borderRadius: 10,
            padding: 15,
            paddingTop: 50,
            width: "100%",
            zIndex: 1,
          }}>
          <Text
            fontWeight="thin"
            style={{
              textAlign: "center",
              color: "#000",
              fontSize: RF(1.4),
              marginHorizontal: "10%",
            }}>
            {item.description}
          </Text>
        </View>
      </CollapseBody>
    </Collapse>
  );
}
