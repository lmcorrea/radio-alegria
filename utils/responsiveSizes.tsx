import { Dimensions, Platform } from "react-native";
import { useCallback, useState, useMemo } from "react";
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";
import { useDimensionsChange } from "react-native-responsive-dimensions";

function responsiveSizesMobile(){
  const width = Dimensions.get("window").width;
  const isMobile = width < 768;
  const RW = (value: number) => responsiveScreenWidth(value);
  const RH = (value: number) => responsiveScreenHeight(value);
  const RF = (value: number) => responsiveScreenFontSize(value);
  return { isMobile, RW, RH, RF };
}

function responsiveSizes() {
  const [width, setWidth] = useState<number>(Dimensions.get("window").width);

  useDimensionsChange(() => {
    setWidth(Dimensions.get("window").width);
  });

  const isMobile = useMemo(() => {
    return width < 768;
  }, [width]);


  const RW = useCallback(
    (value: number) => {
      return responsiveScreenWidth(value);
    },
    [width]
  );

  const RH = useCallback(
    (value: number) => {
      return responsiveScreenHeight(value);
    },
    [width]
  );

  const RF = useCallback(
    (value: number) => {
      return responsiveScreenFontSize(value);
    },
    [width]
  );

  return { isMobile, RW, RH, RF };
}

const responsiveSizesExport = Platform.OS === 'web' ? responsiveSizes : responsiveSizesMobile; 

export default responsiveSizesExport;

