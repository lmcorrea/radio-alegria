declare module "*.png";
declare module "*.svg";
declare module "*.jpeg";
declare module "*.jpg";

declare module "accordion-collapse-react-native" {
  export const Collapse: any;
  export const CollapseHeader: any;
  export const CollapseBody: any;
}