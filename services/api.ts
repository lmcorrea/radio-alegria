import axios from "axios";
import { storage } from "./storage";
import { UserType } from "@/types/RegisterServiceResponse";

let abortController = new AbortController();
let { signal } = abortController;

export const superAudioapi = axios.create({
  baseURL: "https://api.superaudio.com.br/controla/app-superaudio-radios/",
  headers: {
    "Content-Type": "application/json",
  },
});

export const superAudioapiApp = axios.create({
  baseURL: "https://api.superaudio.com.br/api/app/",
  headers: {
    "Content-Type": "application/json",
  },
});

export const baseApiUrl = axios.create({
  baseURL: "https://api.superaudio.com.br/",
  headers: {
    "Content-Type": "application/json",
  },
});

export const authenticatedUrl = axios.create({
  baseURL: "https://api.superaudio.com.br/",
  headers: {
    "Content-Type": "application/json",
  },
});

authenticatedUrl.interceptors.request.use(
  async (config) => {
    const user = storage.getString(`user`);
    const parsedUser: UserType = JSON.parse(user as string);

    if (user) {
      config.headers.Authorization = `Bearer ${parsedUser.token}`;
    }
    return { ...config, signal };
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);
