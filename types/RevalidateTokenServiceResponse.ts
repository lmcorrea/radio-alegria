type City = {
  id: number;
  state_id: number;
  uf: string;
  name: string;
  is_capital: boolean;
};

type State = {
  id: number;
  country_id: number;
  uf: string;
  name: string;
};

type Country = {
  id: number;
  name: string;
  name_en: string;
  alias: string;
  bacen: number;
};

type Location = {
  id: number;
  branch_id: string | null;
  foreign_key: number;
  model: string;
  cep: string | null;
  address: string;
  district: string;
  number: string;
  complement: string | null;
  reference: string | null;
  phone_1: string;
  phone_2: string | null;
  country_id: number;
  city_id: number;
  state_id: number;
  city_name: string | null;
  state_name: string | null;
  lon: string | null;
  lat: string | null;
  run_by: number;
  active: boolean;
  created: string;
  modified: string;
  city: City;
  state: State;
  country: Country;
};

type Musics = {
  rhythms: string[];
  years: string[];
  nationalities: string[];
  tag_styles: string[];
};

type Products = {
  tags: string[];
};

type Preference = {
  musics: Musics;
  products: Products;
};

type Cover = {
  uri: string;
};

type Client = {
  id: number;
  user_type: string;
  type: string;
  name_corporate: string;
  name_fantasy: string;
  register_type: string;
  cnpj: string;
  cpf: string;
  email: string;
  key_remote_access: string | null;
  other_emails: string;
  financial_name: string | null;
  financial_email: string | null;
  financial_observations: string | null;
  payment_due_date: string;
  payment_method: string;
  requester_name: string | null;
  requester_email: string | null;
  requester_bond: string;
  domain_slug: string | null;
  slogan: string | null;
  power_kw: number;
  class: string;
  link: string;
  link_streaming: string | null;
  link_server_rds: string | null;
  type_server_rds: string;
  link_radio_data_system: string | null;
  radio_data_system_id: string | null;
  spotlight: boolean;
  super_spotlight: boolean;
  has_audio: boolean;
  show_radios_online: boolean;
  status_show_radios_online: number;
  about: string;
  observations: string | null;
  incomplete_registration: boolean;
  incomplete_registration_alert: string | null;
  run_by: number;
  status: number;
  show_music_play: boolean;
  registered_by: number;
  first_access: string;
  last_access: string;
  validate_access: string;
  uuid: string;
  is_public: number;
  notify_content_update: boolean;
  receive_mailing: boolean;
  show_logo_radio_subscriber: boolean;
  accepted_terms_of_use: boolean;
  advertiser_segment_id: string | null;
  enable_automatic_product_block: boolean;
  request_from: string;
  created: string;
  modified: string;
  clients_emails: string[];
  location: Location;
  token: string;
  client_api: string | null;
  other_cities: string[];
  radio_segments: string | null;
  preference: Preference;
  cover: Cover;
  has_favorites: boolean;
  radio_type: string;
  accessProfileScopes: string[];
};

export type RevalidateTokenServiceResponse = {
  client: Client;
  status: string;
};
